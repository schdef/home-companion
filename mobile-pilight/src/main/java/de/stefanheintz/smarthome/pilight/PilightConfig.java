package de.stefanheintz.smarthome.pilight;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import de.stefanheintz.smarthome.ServiceNotAvailable;
import de.stefanheintz.smarthome.config.AbstractConfig;
import de.stefanheintz.smarthome.config.Config;
import de.stefanheintz.smarthome.config.RawConfig;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class PilightConfig extends AbstractConfig implements Config {

    private static final String DEFAULT_HOST = "192.168.178.33";
    private static final String DEFAULT_PORT = "5001";
    private static PilightVersion DETECTED_PILIGHT_VERSION;

    public PilightConfig(Context context) {
        super(context, DEFAULT_HOST, DEFAULT_PORT);
    }

    private static PilightVersion getDetectedPilightVersion() {
        return DETECTED_PILIGHT_VERSION;
    }

    private List<SmartHomeDevice> parseVersion6(JSONObject config) {
        List<SmartHomeDevice> devices = new ArrayList<>();
        try {
            JSONObject jsonDevices = config.getJSONObject("devices");
            Iterator<String> jsonKeys = jsonDevices.keys();
            while (jsonKeys.hasNext()) {
                String element = (String) jsonKeys.next();

                JSONObject jsonDevice = jsonDevices.getJSONObject(element);
                Iterator<?> jsonDeviceKeys = jsonDevice.keys();

                String stateValue = null;
                String temperatureValue = null;
                String humidityValue = null;
                while (jsonDeviceKeys.hasNext()) {
                    try {
                        String dkey = (String) jsonDeviceKeys.next();
                        if (dkey.equals("temperature")) {
                            temperatureValue = jsonDevice.getString(dkey);
                        } else if (dkey.equals("state")) {
                            stateValue = jsonDevice.getString(dkey);
                        } else if (dkey.equals("humidity")) {
                            humidityValue = jsonDevice.getString(dkey);
                        }
                    } catch (JSONException e) {
                        //Log.w(TAG, "The received DEVICE is of an incorrent format");
                    }
                }
                SmartHomeDevice device;
                if(temperatureValue != null) {
                    device = new TemperatureDevice();
                    ((TemperatureDevice)device).setTemperature(temperatureValue);
                    ((TemperatureDevice)device).setHumidity(humidityValue);
                } else {
                    device = new SwitchDevice();
                    device.setState(stateValue);
                }
                device.setName(element);

                devices.add(device);
                DETECTED_PILIGHT_VERSION = PilightVersion.VERSION_SIX;
            }

            for (SmartHomeDevice device : devices) {
                JSONObject jsonGui = config.getJSONObject("gui");
                Iterator<?> jsonGuiKeys = jsonGui.keys();
                while (jsonGuiKeys.hasNext()) {
                    String element = (String) jsonGuiKeys.next();
                    if (element.equals(device.getName())) {
                        JSONObject jsonGuiDevice = jsonGui.getJSONObject(element);
                        String jsonGuiDeviceNameAlias = jsonGuiDevice.getString("name");
                        if (jsonGuiDeviceNameAlias != null) {
                            device.setAlias(jsonGuiDeviceNameAlias);
                        }


                        JSONArray jsonArrayGroup = jsonGuiDevice.getJSONArray("group");
                        if (jsonArrayGroup != null && jsonArrayGroup.length() > 0) {
                            if (!jsonArrayGroup.isNull(0)) {
                                String firstRoom = (String) jsonArrayGroup.get(0);
                                if (firstRoom != null) {
                                    device.setLocations(parseRoomString(firstRoom));
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            //Log.w(TAG, "The received LOCATION is of an incorrent format");
            e.printStackTrace();
        }



        return devices;
    }

    private List<SmartHomeDevice> parseVersion5(JSONObject config) {
        List<SmartHomeDevice> devices = new ArrayList<>();
        Iterator<?> jsonKeys = config.keys();
        while (jsonKeys.hasNext()) {
            String locationID = (String) jsonKeys.next();
            try {
                JSONObject jsonLocation = config.getJSONObject(locationID);
                Iterator<?> jsonLocationKeys = jsonLocation.keys();

                while (jsonLocationKeys.hasNext()) {
                    String dkey = (String) jsonLocationKeys.next();
                    if (!dkey.equals("name")) {
                        try {
                            SmartHomeDevice device = new SwitchDevice();
                            device.setName(dkey);
                            device.setLocations(parseRoomString(locationID));

                            JSONObject jsonDevice = jsonLocation.getJSONObject(dkey);
                            Iterator<String> jsonDeviceKeys = jsonDevice.keys();

                            while (jsonDeviceKeys.hasNext()) {
                                String skey = jsonDeviceKeys.next();
                                if (skey.equals("state")) {
                                    device.setState(jsonDevice.getString(skey));
                                }
                            }
                            devices.add(device);
                        } catch (JSONException e) {
                            //Log.w(TAG, "The received DEVICE is of an incorrent format");
                        }
                    }
                }
                DETECTED_PILIGHT_VERSION = PilightVersion.VERSION_FIVE;
            } catch (JSONException e) {
                //Log.w(TAG, "The received LOCATION is of an incorrent format");
            }
        }
        return devices;
    }

    @Override
    public List<SmartHomeDevice> getDevices() throws ServiceNotAvailable {
        return getDevices("http://" + HOST + ":" + PORT + "/config");
    }

    private String buildSwitchUrl(SmartHomeDevice device, PilightVersion version) {
        String state = device.getState();
        String location = device.getLocationForSwitchOrEmptyString();
        String deviceName = device.getName();
        state = inverseState(state);

        String url = "http://" + HOST + ":" + PORT + "/send?%7B%22message%22%3A%22send%22%2C%22code%22%3A%7B%22location%22%3A%22" + location + "%22%2C%22device%22%3A%22" + deviceName + "%22%2C%22state%22%3A%22" + state + "%22%7D%7D";
        if (PilightVersion.VERSION_SIX.equals(version)) {
            url = "http://" + HOST + ":" + PORT + "/send?" + "%7B%22action%22%3A%22control%22%2C%22code%22%3A%7B%22device%22%3A%22" + deviceName + "%22%2C%22state%22%3A%22" + state + "%22%7D%7D";
        }

        return url;
    }

    @Override
    public String getImplementationName() {
        return "pilight";
    }

    @Override
    protected List<SmartHomeDevice> parseConfig(RawConfig rawConfig) throws JSONException {
        JSONObject jsonConfig = new JSONObject(rawConfig.getItemsConfig());
        List<SmartHomeDevice> devices = Collections.EMPTY_LIST;
        if (rawConfig != null && rawConfig.getItemsConfig() != null && rawConfig.getItemsConfig().contains("config")) {
            devices = parseVersion5(jsonConfig.getJSONObject("config"));
            //Log.d(TAG, "parsing version 5");
        } else if (rawConfig != null && rawConfig.getItemsConfig() != null && rawConfig.getItemsConfig().contains("devices")) {
            //Log.d(TAG, "parsing version 6");
            devices = parseVersion6(jsonConfig);
        }
        Collections.sort(devices);
        return devices;
    }

    @Override
    public void executeDeviceAction(SmartHomeDevice device, String wearCommand) throws ServiceNotAvailable {
        String url = buildSwitchUrl(device, getDetectedPilightVersion());
        httpGetRequest(url);
    }

    public enum PilightVersion {
        VERSION_FIVE,
        VERSION_SIX;

        public static PilightVersion latest() {
            return VERSION_SIX;
        }
    }
}


