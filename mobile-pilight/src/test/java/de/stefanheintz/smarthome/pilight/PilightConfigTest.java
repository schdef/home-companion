package de.stefanheintz.smarthome.pilight;

import android.content.SharedPreferences;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowPreferenceManager;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import de.stefanheintz.smarthome.AbstractConfigTest;
import de.stefanheintz.smarthome.ServiceNotAvailable;
import de.stefanheintz.smarthome.config.AbstractConfig;
import de.stefanheintz.smarthome.config.BackendConfig;
import de.stefanheintz.smarthome.config.Credential;
import de.stefanheintz.smarthome.config.HttpCommunicationGateway;
import de.stefanheintz.smarthome.dto.GroupingUtil;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by Stefan on 01.03.2015.
 */
@RunWith(RobolectricTestRunner.class)
public class PilightConfigTest extends AbstractConfigTest {

    @Before
    public void before() {
        ClassLoader classLoader = getClass().getClassLoader();
        String fileName = "pilight6.config";
        File file = new File(classLoader.getResource(fileName).getFile());
        CONFIG_DIR = file.getPath().replace(fileName, "");
    }

    private String readPilight5Config() throws IOException {
        return readConfigFile(new File(CONFIG_DIR + "/pilight-5.0.config")).getItemsConfig();
    }

    private String readPilight6Config() throws IOException {
        return readConfigFile(new File(CONFIG_DIR + "/pilight6.config")).getItemsConfig();
    }

    private String readPilight6WeatherConfig() throws IOException {
        return readConfigFile(new File(CONFIG_DIR + "/pilight-6.0-weather.json")).getItemsConfig();
    }

    @Test
    public void test_successful_config_parsing_with_pilight_5_version() throws Exception {
        test_successful_config_parsing(getDefaultConfig(PilightConfig.PilightVersion.VERSION_FIVE), 6);
    }

    @Test
    public void test_successful_config_parsing_with_pilight_6_version() throws Exception {
        test_successful_config_parsing(getDefaultConfig(PilightConfig.PilightVersion.VERSION_SIX), 6);
    }

    @Test
    public void test_successful_config_parsing_with_pilight_6_version_and_weather() throws Exception {
        List<SmartHomeDevice> devices = test_successful_config_parsing(readPilight6WeatherConfig(), 7);
        int switchDeviceCounter = 0;
        int temperatureDeviceCounter = 0;
        for (SmartHomeDevice d : devices) {
            if(d instanceof SwitchDevice) {
                switchDeviceCounter++;
            } else if(d instanceof TemperatureDevice) {
                temperatureDeviceCounter++;
            }
        }
        Assert.assertEquals(1, temperatureDeviceCounter);
    }

    private List<SmartHomeDevice> test_successful_config_parsing(final String configJSON, int expectedDevices) throws Exception {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "pilight").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return configJSON;
            }
        });
        List<SmartHomeDevice> devices = config.getDevices();
        assertEquals("pilight devices", expectedDevices, devices.size());
        return devices;
    }

    @Test
    public void test_successful_grouping() throws Exception {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "pilight").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return getDefaultConfig(PilightConfig.PilightVersion.VERSION_SIX);
            }
        });
        List<SmartHomeDevice> deviceList = config.getDevices();
        assertEquals("pilight devices", 6, deviceList.size());

        Map<SmartHomeLocation, List<SmartHomeDevice>> map = GroupingUtil.groupByLocation(deviceList);
        map.put(new SmartHomeLocation("All"), deviceList);

        for (Map.Entry<SmartHomeLocation, List<SmartHomeDevice>> smartHomeLocationListEntry : map.entrySet()) {
            List<SmartHomeDevice> value = smartHomeLocationListEntry.getValue();
            for (SmartHomeDevice smartHomeDevice : value) {
                System.out.println(smartHomeDevice.getName() + ":" + smartHomeDevice.getAlias());
            }
            System.out.println(smartHomeLocationListEntry.getKey().getName() + ":" + value.size());
        }
    }


    @Test
    public void test_default_home_element_are_available_version_pilight5() throws Exception {
        test_default_home_element_are_available(PilightConfig.PilightVersion.VERSION_FIVE);
    }

    @Test
    public void test_default_home_element_are_available_version_pilight6() throws Exception {
        test_default_home_element_are_available(PilightConfig.PilightVersion.VERSION_SIX);
    }

    private void test_default_home_element_are_available(final PilightConfig.PilightVersion version) throws Exception {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "pilight").commit();

        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return getDefaultConfig(version);
            }
        });
        List<SmartHomeDevice> devices = config.getDevices();
        checkForElement(devices, "Regal");
        checkForElement(devices, "Stehlampe");
        checkForElement(devices, "Entertainment");
    }

    private void checkForElement(List<SmartHomeDevice> devices, String elementToCheck) {
        boolean found = false;
        for (SmartHomeDevice device : devices) {
            if (elementToCheck.equals(device.getName())) {
                found = true;
            }
        }
        assertTrue("element " + elementToCheck + " not found", found);
    }

    private String getDefaultConfig(PilightConfig.PilightVersion version) {
        if (version.equals(PilightConfig.PilightVersion.VERSION_FIVE)) {
            try {
                return readPilight5Config();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        try {
            return readPilight6Config();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    public void test_for_switch() throws ServiceNotAvailable, Exception {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "pilight").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return getDefaultConfig(PilightConfig.PilightVersion.VERSION_SIX);
            }
        });

        config.executeDeviceAction("Brenn12B");
    }
}
