package de.stefanheintz.smarthome;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.config.BackendConfig;
import de.stefanheintz.smarthome.config.Config;
import de.stefanheintz.smarthome.pilight.PilightConfig;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class WearListenerService extends WearableListenerService {

    private static final String TAG = "WearListenerService";
    private GoogleAPIHelper googleAPIHelper;
    private Analytics analyticsTracker;
    Config config;
    private SharedPreferences sharedPreferences;
    private boolean isDebugMode;

    @Override
    public void onCreate() {
        super.onCreate();
        googleAPIHelper = new GoogleAPIHelper(this, this);
        config = BackendConfig.getInstance(getApplicationContext());
        MainApplication application = (MainApplication) getApplication();
        analyticsTracker = application.getDefaultTracker();
        analyticsTracker.setScreenName("WEAR~" + this.getClass().getSimpleName() + "~" + config.getImplementationName() + "~" + ((config != null && config.getLastRetrievedDevices() != null) ? config.getLastRetrievedDevices().size() : "?"));
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        isDebugMode = sharedPreferences.getBoolean(Settings.PREF_KEY_EN_DIS_DEBUG_MODE, false);
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if ("/MESSAGE".equals(messageEvent.getPath())) {
            String deviceNameId = new String(messageEvent.getData());
            Log.v(TAG, "MESSAGE message from wear with path: " + messageEvent.getPath() + ", device: " + deviceNameId);
            if (deviceNameId.toLowerCase().contains("refresh") || deviceNameId.toLowerCase().contains("swipe")) {
//                Log.v(TAG, "MESSAGE from wear is for refreshing");
                new Runnable() {
                    @Override
                    public void run() {
                        if (config instanceof PilightConfig) {
                            try {
                                Thread.sleep(1500L);
                            } catch (InterruptedException e) {
                            }
                        }
                        if(isDebugMode) {
                            Toast.makeText(getApplicationContext(), "Action received for get all devices", Toast.LENGTH_LONG).show();
                        }
                        long startTime = System.currentTimeMillis();
                        googleAPIHelper.publishList(null, sharedPreferences);
                        analyticsTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("Action")
                                .setAction("Refresh")
                                .setLabel("Devicelist")
                                .setValue(System.currentTimeMillis() - startTime)
                                .build());
                    }
                }.run();
            } else {
                try {
                    Log.v(TAG, "Actionrequest received for " + deviceNameId);
                    if(isDebugMode) {
                        Toast.makeText(getApplicationContext(), "Actionrequest received for " + deviceNameId, Toast.LENGTH_LONG).show();
                    }
                    long startTime = System.currentTimeMillis();
                    config.executeDeviceAction(deviceNameId);
                    googleAPIHelper.publishDevice(deviceNameId.split("~")[0]);
                    analyticsTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Click")
                            .setLabel("ExecuteDeviceAction")
                            .setValue(System.currentTimeMillis() - startTime)
                            .build());
                } catch (ServiceNotAvailable e) {
                    //Log.e(TAG, "exception: " + e.getMessage());
                }
            }
        } else if ("/GETDEVICELIST".equals(messageEvent.getPath())) {
            String data = new String(messageEvent.getData());
            Log.v(TAG, "GETDEVICELIST message from wear is: " + data);
            new Runnable() {
                @Override
                public void run() {

                    if(isDebugMode) {
                        Toast.makeText(getApplicationContext(), "Action received for get all devices", Toast.LENGTH_LONG).show();
                    }
                    long startTime = System.currentTimeMillis();
                    if (config instanceof PilightConfig) {
                        try {
                            Thread.sleep(1500L);
                        } catch (InterruptedException e) {
                        }
                    }
                    googleAPIHelper.publishList(null, sharedPreferences);
                    analyticsTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Refresh")
                            .setLabel("Devicelist")
                            .setValue(System.currentTimeMillis() - startTime)
                            .build());
                }
            }.run();

        } else if ("/GETDEVICE".equals(messageEvent.getPath())) {
            final String deviceId = new String(messageEvent.getData());
            Log.v(TAG, "GETDEVICE received for " + deviceId);
            new Runnable() {
                @Override
                public void run() {

                    if(isDebugMode) {
                        Toast.makeText(getApplicationContext(), "Action received for get device " + deviceId, Toast.LENGTH_LONG).show();
                    }
                    long startTime = System.currentTimeMillis();
                    if (config instanceof PilightConfig) {
                        try {
                            Thread.sleep(1500L);
                        } catch (InterruptedException e) {
                        }
                    }
                    googleAPIHelper.publishDevice(deviceId);
                    analyticsTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Refresh")
                            .setLabel("Device")
                            .setValue(System.currentTimeMillis() - startTime)
                            .build());
                    if(isDebugMode) {
                        Toast.makeText(getApplicationContext(), "Action received for get device " + deviceId + " took " + (System.currentTimeMillis() - startTime) +"ms", Toast.LENGTH_LONG).show();
                    }
                }
            }.run();

        } else if("/GETSETTINGS".equals(messageEvent.getPath())) {
            Log.v(TAG, "GETSETTINGS received");
            long startTime = System.currentTimeMillis();
            googleAPIHelper.publishSettings(sharedPreferences);
            analyticsTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Refresh")
                    .setLabel("Settings")
                    .setValue(System.currentTimeMillis() - startTime)
                    .build());
        } else {
            super.onMessageReceived(messageEvent);
        }
    }
}