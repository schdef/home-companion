package de.stefanheintz.smarthome;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.stefanheintz.smarthome.common.TemperatureUtil;
import de.stefanheintz.smarthome.config.BackendConfig;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;

/**
 * Created by Stefan on 22.11.2015.
 */
public class TemperatureHeaterSetupFragment extends DialogFragment implements View.OnClickListener{

    private static final float DEFAULT_VALUE = 13.5f;
    public TemperatureHeatingDevice device;
    private TextView currentTempTextView;
    private TextView desiredTempTextView;
    private TextView plusTextView;
    private TextView minusTextView;
    private TextView confirmTextView;
    private TextView vLabel;

    public static TemperatureHeaterSetupFragment newInstance(TemperatureHeatingDevice device) {
        TemperatureHeaterSetupFragment fragment = new TemperatureHeaterSetupFragment();
        Bundle args = new Bundle();
        args.putParcelable("DEVICE", device);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.temperature_heating_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vLabel = (TextView) view.findViewById(R.id.label);
        currentTempTextView = (TextView) view.findViewById(R.id.currentTemp);
        desiredTempTextView = (TextView) view.findViewById(R.id.desiredTemp);
        plusTextView = (TextView) view.findViewById(R.id.plus);
        minusTextView = (TextView) view.findViewById(R.id.minus);
        confirmTextView = (TextView) view.findViewById(R.id.confirm);

        TemperatureHeatingDevice deviceParcel = getArguments().getParcelable("DEVICE");
        if(deviceParcel != null) {
            this.device = deviceParcel;
        }
        update(this.device);

        plusTextView.setOnClickListener(this);
        minusTextView.setOnClickListener(this);
        confirmTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(plusTextView)){
            float desiredTemp = readDesiredTemp();
            desiredTemp += 0.5;
            desiredTempTextView.setText(String.valueOf(desiredTemp)+ TemperatureUtil.DEGREE_SIGN);

        } else if(v.equals(minusTextView)){
            float desiredTemp = readDesiredTemp();
            desiredTemp -= 0.5;
            desiredTempTextView.setText(String.valueOf(desiredTemp)+ TemperatureUtil.DEGREE_SIGN);

        } else if(v.equals(confirmTextView)){
            device.setDesiredTemperature(String.valueOf(readDesiredTemp()));
            new Thread() {
                @Override
                public void run() {
                    try {
                        BackendConfig.getInstance(getActivity().getApplicationContext()).executeDeviceAction(device, device.getDesiredTemperature());
                    } catch (ServiceNotAvailable serviceNotAvailable) {

                    }
                }
            }.start();
            dismiss();
        }
    }

    private float readDesiredTemp() {

        String s = desiredTempTextView.getText().toString();
        String replaced = s.replace(TemperatureUtil.DEGREE_SIGN, "").replace("off", ""+DEFAULT_VALUE);
        try {
            return Float.parseFloat(replaced);
        } catch(Exception e) {
            return 13.5f;
        }
    }

    public void update(SmartHomeDevice newDevice) {
        device = (TemperatureHeatingDevice) newDevice;
        vLabel.setText(device.getAliasOrName());
        currentTempTextView.setText(device.getMeasuredTemperature()+TemperatureUtil.DEGREE_SIGN);
        desiredTempTextView.setText(TemperatureUtil.formattedTemperatureValue(device.getDesiredTemperature()) + TemperatureUtil.DEGREE_SIGN);
    }
}