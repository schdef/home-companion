package de.stefanheintz.smarthome;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.config.BackendConfig;
import de.stefanheintz.smarthome.config.Config;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;

/**
 * The MIT License (MIT)
 * <p/>
 * Copyright (c) 2015 Stefan Heintz
 * <p/>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p/>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p/>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class GoogleAPIHelper implements
        GoogleApiClient.ConnectionCallbacks {

    private final DataApi.DataListener listener;
    private final Context context;
    private GoogleApiClient mGoogleApiClient;
    private Config config;

    public GoogleAPIHelper(Context context, DataApi.DataListener listener) {
        this.listener = listener;
        this.context = context;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
        config = BackendConfig.getInstance(context);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Wearable.DataApi.addListener(mGoogleApiClient, listener);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Wearable.DataApi.removeListener(mGoogleApiClient, listener);
        mGoogleApiClient.disconnect();
    }

    public void publishSettings(final SharedPreferences preferences) {
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/SETTINGS_RESPONSE");
        DataMap map = putDataMapReq.getDataMap();
        updatePreferenceSettings(map, preferences);
        map.putLong("time", System.nanoTime());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);
    }

    private void updatePreferenceSettings(DataMap map, SharedPreferences preferences) {
        map.putString(Settings.PREF_KEY_NO_FULL_OR_LIST_VIEW, preferences.getString(Settings.PREF_KEY_NO_FULL_OR_LIST_VIEW, Settings.PREF_DEFAULT_NO_FULL_OR_LIST_VIEW));
        map.putBoolean(Settings.PREF_KEY_EN_DIS_DEBUG_MODE, preferences.getBoolean(Settings.PREF_KEY_EN_DIS_DEBUG_MODE, false));
        map.putString(Settings.PREF_KEY_ROOM_FILTER, preferences.getString(Settings.PREF_KEY_ROOM_FILTER, null));
    }

    public void publishList(List<SmartHomeDevice> devices, final SharedPreferences preferences) {
        //Log.d("DEBUG", "publishList start");
        if (devices == null) {
            devices = Collections.EMPTY_LIST;
            try {
                devices = config.getDevices();
            } catch (ServiceNotAvailable serviceNotAvailable) {
            }
        }
        List<SmartHomeDevice> filteringList = new ArrayList();
        for (SmartHomeDevice device : devices) {
            if (device.isVisible()) {
                filteringList.add(device);
            }
        }

        final ArrayList<DataMap> deviceListDataMap = getAsDataMapList(filteringList);
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/devicelist");
        DataMap map = putDataMapReq.getDataMap();
        map.putDataMapArrayList("devicelist", deviceListDataMap);
        updatePreferenceSettings(map, preferences);
        map.putLong("time", System.nanoTime());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();

        Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);
                /*
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq).setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                    @Override
                    public void onResult(DataApi.DataItemResult dataItemResult) {
                        Log.d(TAG, "publishList onResult");
                        if(!dataItemResult.getStatus().isSuccess()) {
                            Log.d(TAG, "publishList onResult not successful");
                        } else {
                            Log.d(TAG, "publishList onResult successful");
                        }
                    }
                });
                */
        //Log.d(TAG, "publishList end");
    }

    private ArrayList<DataMap> getAsDataMapList(List<SmartHomeDevice> devices) {
        ArrayList<DataMap> result = new ArrayList<>();
        for (SmartHomeDevice device : devices) {
            result.add(device.toDataMap());
        }
        return result;
    }

    public void publishDevice(String deviceId) {
        SmartHomeDevice device = null;
        try {
            device = config.getDevice(deviceId);
        } catch (ServiceNotAvailable serviceNotAvailable) {
        }
        final SmartHomeDevice deviceForTransport = device;
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/GETDEVICE_RESPONSE");
        DataMap map = putDataMapReq.getDataMap();
        map.putDataMap("device", deviceForTransport.toDataMap());
        map.putLong("time", System.nanoTime());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);
    }
}
