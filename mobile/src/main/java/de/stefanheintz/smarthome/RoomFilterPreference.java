package de.stefanheintz.smarthome;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.config.BackendConfig;
import de.stefanheintz.smarthome.dto.GroupingUtil;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class RoomFilterPreference extends DialogPreference {

    private static final String NO_ROOM = "remove filter";

    final ArrayAdapter<String> adapter;

    public RoomFilterPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setPersistent(false);
        setDialogLayoutResource(R.layout.roomfilter_dialog);
        setPositiveButtonText("OK");
        setNegativeButtonText("Cancel");

        List<String> roomList = new ArrayList<>();
        try {
            List devices = BackendConfig.getInstance(getContext()).getLastRetrievedDevices();
            Map<SmartHomeLocation, List<SmartHomeDevice>> map = GroupingUtil.groupByLocation(devices, false);
            roomList.add(NO_ROOM);
            for (SmartHomeLocation smartHomeLocation : map.keySet()) {
                roomList.add(smartHomeLocation.getName());
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), "Could not receive room list. Error is: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, roomList);
    }

    @Override
    protected void onBindDialogView(View view) {
        ListView listView = (ListView) view.findViewById(R.id.listView2);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = getEditor();
                String item = adapter.getItem(position);
                if (item == null || item.equals(NO_ROOM)) {
                    editor.putString(Settings.PREF_KEY_ROOM_FILTER, null);
                } else {
                    editor.putString(Settings.PREF_KEY_ROOM_FILTER, item);
                }
                editor.commit();
                getDialog().cancel();
            }
        });

        super.onBindDialogView(view);
    }

}
