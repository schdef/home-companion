package de.stefanheintz.smarthome;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;

import com.google.android.gms.analytics.HitBuilders;

import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.config.BackendConfig;


/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class SettingsFragment extends PreferenceFragment implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    private Analytics analyticsTracker;
    EditTextPreference systemConfigPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        ListPreference systemPreferenceList = (ListPreference) getPreferenceManager().findPreference(Settings.PREF_KEY_SYSTEM_PREFERENCE);
        systemConfigPreference = (EditTextPreference) getPreferenceManager().findPreference(Settings.PREF_KEY_SYSTEM_CONFIG);
        systemPreferenceList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                handleSystemConfig(o.toString());
                return true;
            }
        });
        initSummary(getPreferenceScreen());
        handleSystemConfig(systemPreferenceList.getValue());
    }

    private void handleSystemConfig(String o) {
        if(o.toLowerCase().equals("openhab")) {
            systemConfigPreference.setEnabled(true);
        } else {
            systemConfigPreference.setEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
        MainApplication application = (MainApplication) getActivity().getApplication();
        analyticsTracker = application.getDefaultTracker();
        analyticsTracker.setScreenName("MOBILE~" + this.getClass().getSimpleName() + "~" + BackendConfig.getInstance(getActivity().getApplicationContext()).getImplementationName());
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        analyticsTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Settings-Change")
                .setLabel(key)
                .build());
        updatePrefSummary(findPreference(key));
    }

    private void initSummary(Preference p) {
        if (p instanceof PreferenceGroup) {
            PreferenceGroup pGrp = (PreferenceGroup) p;
            for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
                initSummary(pGrp.getPreference(i));
            }
        } else {
            updatePrefSummary(p);
        }
    }

    private void updatePrefSummary(Preference p) {
        if (p instanceof ListPreference) {
            ListPreference listPref = (ListPreference) p;
            p.setSummary(listPref.getEntry());
        }
        if (p instanceof EditTextPreference) {
            EditTextPreference editTextPref = (EditTextPreference) p;
            if (p.getTitle().toString().contains("assword")) {
                p.setSummary("******");
            } else {
                p.setSummary(editTextPref.getText());
            }
        }
        if (p instanceof MultiSelectListPreference) {
            EditTextPreference editTextPref = (EditTextPreference) p;
            p.setSummary(editTextPref.getText());
        }

        if (p instanceof RoomFilterPreference) {
            String value = getPreferenceScreen().getSharedPreferences().getString(Settings.PREF_KEY_ROOM_FILTER, null);
            if (value == null) {
                p.setSummary(R.string.pref_summary_room_filter);
            } else {
                p.setSummary(value);
            }
        }
    }
}
