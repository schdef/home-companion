package de.stefanheintz.smarthome;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class HelpSlides extends AppIntro {

    // Please DO NOT override onCreate. Use init
    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(AppIntroFragment.newInstance("Smartphone settings", "Configure the app in the settings menu.", R.drawable.help_settings_menu, getResources().getColor(R.color.slide_light_green)));
        addSlide(AppIntroFragment.newInstance("SmartHome system", "Choose your backend server type.", R.drawable.help_choose_system, getResources().getColor(R.color.slide_green)));
        addSlide(AppIntroFragment.newInstance("Backend communication", "Configure the server address, port and communication type.", R.drawable.help_choose_ip_and_port, getResources().getColor(R.color.slide_teal)));
        addSlide(AppIntroFragment.newInstance("Smartphone app", "You can see the current temperature of a device or switch devices on/off by tapping the button.", R.drawable.help_smartphone_device_list, getResources().getColor(R.color.slide_cyan)));
        addSlide(AppIntroFragment.newInstance("Settings for the watch", "The blue eye means, the device is visible on the watch.\nThe yellow star means, the device is marked as favorite device.", R.drawable.help_available_types_smartphone, getResources().getColor(R.color.slide_blue)));
        addSlide(AppIntroFragment.newInstance("Rooms on the watch", "Devices on the watch are grouped in rooms and you can swipe up or down through the list of rooms.", R.drawable.help_living_room, getResources().getColor(R.color.slide_indigo)));
        addSlide(AppIntroFragment.newInstance("Favorites on the watch", "Devices marked in the smartphone app as favorite and shown in an own room.", R.drawable.help_watch_fav, getResources().getColor(R.color.slide_purple)));
        addSlide(AppIntroFragment.newInstance("Switch a device", "You can switch a device on or off by tapping on it.", R.drawable.help_entertainment_fullscreen, getResources().getColor(R.color.slide_violet)));
        addSlide(AppIntroFragment.newInstance("Temperature device", "The left circle shows the temperature and the right circle the humidity of a device.", R.drawable.help_temperature, getResources().getColor(R.color.slide_pink)));
        addSlide(AppIntroFragment.newInstance("Watch app", "If there are more than 3 devices in a room, the devices are shown as list.", R.drawable.help_watch_device_list, getResources().getColor(R.color.slide_red)));

        // OPTIONAL METHODS
        // Override bar/separator color
        //setBarColor(Color.parseColor("#3F51B5"));
        //setSeparatorColor(Color.parseColor("#2196F3"));

        showDoneButton(true);
        showSkipButton(true);
        setFadeAnimation();
    }

    @Override
    public void onSkipPressed() {
        finish();
    }

    @Override
    public void onDonePressed() {
        // Do something when users tap on Done button.
        finish();
    }
}