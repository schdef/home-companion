package de.stefanheintz.smarthome;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.stefanheintz.smarthome.common.TemperatureUtil;
import de.stefanheintz.smarthome.config.BackendConfig;
import de.stefanheintz.smarthome.config.Config;
import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;
import de.stefanheintz.smarthome.pilight.PilightConfig;

import static android.R.attr.data;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class MainActivity extends Activity implements DataApi.DataListener {

    public static final int ON_SETTING_RESULT = 1;
    private static final String TAG = "MainActivity";
    private static final int REQUEST_INVITE = 0;

    SwipeRefreshLayout.OnRefreshListener listener = null;
    private ListView mListView = null;
    private DeviceAdapter mAdapter = null;
    private SwipeRefreshLayout mSwipeRefreshLayout = null;
    private CheckBox visibilityCheckBox = null;
    private CheckBox favouriteCheckBox = null;

    private GoogleAPIHelper googleAPIHelper;
    private Analytics analyticsTracker;
    long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-9847638548072818~1854992727");

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mListView = (ListView) findViewById(R.id.listView);
        mListView.setFastScrollAlwaysVisible(true);

        //Initialize swipe to refresh view
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        listener = newOnRefreshListener();
        mSwipeRefreshLayout.setOnRefreshListener(listener);

        // Initialize components
        visibilityCheckBox = (CheckBox) findViewById(R.id.visibilityAllCheckBox);
        if(visibilityCheckBox != null) {
            visibilityCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeVisibilityAll();
                }
            });
        }

        favouriteCheckBox = (CheckBox) findViewById(R.id.favouriteAllCheckBox);
        if(favouriteCheckBox != null) {
            favouriteCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeSettingFavouriteAll();
                }
            });
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String systemSetting = preferences.getString("system_preference", null);
        if(systemSetting != null) {
            listener.onRefresh();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setActionBar(toolbar);
            getActionBar().setTitle(getResources().getString(R.string.app_name));
        }
        if (googleAPIHelper == null) {
            googleAPIHelper = new GoogleAPIHelper(getApplicationContext(), this);
        }

        new EulaDialog(this).showAndAsk();

    }

    private void changeSettingFavouriteAll() {
        if (mListView != null && mListView.getAdapter() != null && !mListView.getAdapter().isEmpty()) {
            boolean currentState = favouriteCheckBox.isChecked();
            if(currentState) {
                favouriteCheckBox.setText(R.string.fav_all_devices);
            } else {
                favouriteCheckBox.setText(R.string.fav_all_none_devices);
            }

            DeviceAdapter adapter = (DeviceAdapter) mListView.getAdapter();
            SmartHomeDevice[] smartHomeDevices = adapter.data;
            for (SmartHomeDevice device : smartHomeDevices) {
                device.setFav(currentState);
            }

            List<SmartHomeDevice> devices = Arrays.asList(smartHomeDevices);
            updateDevicesSettings(devices);
            updateList(null, devices);
            analyticsTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Update")
                    .setLabel("Fav")
                    .build());
        }
    }

    private void changeVisibilityAll() {
        if (mListView != null && mListView.getAdapter() != null && !mListView.getAdapter().isEmpty()) {
            boolean currentState = visibilityCheckBox.isChecked();
            if(currentState) {
                visibilityCheckBox.setText(R.string.visibility_all_show_all_devices);
            } else {
                visibilityCheckBox.setText(R.string.visibility_all_hide_all_devices);
            }

            DeviceAdapter adapter = (DeviceAdapter) mListView.getAdapter();
            SmartHomeDevice[] smartHomeDevices = adapter.data;
            for (SmartHomeDevice device : smartHomeDevices) {
                device.setVisible(currentState);
            }

            List<SmartHomeDevice> devices = Arrays.asList(smartHomeDevices);
            updateDevicesSettings(devices);
            updateList(null, devices);
            analyticsTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Update")
                    .setLabel("Visibility")
                    .build());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication application = (MainApplication) getApplication();
        Config config = BackendConfig.getInstance(getApplicationContext());
        analyticsTracker = application.getDefaultTracker();
        analyticsTracker.setScreenName("MOBILE~" + this.getClass().getSimpleName() + "~" + config.getImplementationName() + "~" + ((config != null && config.getLastRetrievedDevices() != null) ? config.getLastRetrievedDevices().size() : "?"));
        analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private SwipeRefreshLayout.OnRefreshListener newOnRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new RefreshDeviceListTask().execute();
            }
        };
    }

    private void updateList(final Throwable error, SmartHomeDevice newDevice) {
        if (error != null) {
            DeviceAdapter mAdapter = new DeviceAdapter(MainActivity.this, R.layout.listview_item_row, new SmartHomeDevice[]{});
            mListView.setAdapter(null);
            runOnUiThread(new Runnable() {
                public void run() {
                    createDialog("Error occured", error.getMessage()).show();
                    //Toast.makeText(getApplicationContext(), serviceNotAvailable.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            ((DeviceAdapter) mListView.getAdapter()).refill(newDevice);
        }
        analyticsTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Refresh")
                .setLabel("Device")
                .setValue(System.currentTimeMillis() - startTime)
                .build());
    }

    private void updateList(final Throwable error, List<SmartHomeDevice> newDevices) {
        if (error != null) {
            mListView.setAdapter(null);
            runOnUiThread(new Runnable() {
                public void run() {
                    createDialog("Error occured", error.getMessage()).show();
                    //Toast.makeText(getApplicationContext(), serviceNotAvailable.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            if (mListView.getAdapter() == null) {
                DeviceAdapter mAdapter = new DeviceAdapter(MainActivity.this, R.layout.listview_item_row, newDevices.toArray(new SmartHomeDevice[]{}));
                mListView.setAdapter(mAdapter);
            } else {
                ((DeviceAdapter) mListView.getAdapter()).refill(newDevices);
            }
        }
        //devices = new ArrayList(newDevices);

        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        analyticsTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Refresh")
                .setLabel("Devicelist")
                .setValue(System.currentTimeMillis() - startTime)
                .build());
    }


    public void onItemClickForSwitch(final SmartHomeDevice device) {
        if (device != null) {
            //Log.i(this.getClass().getName(), "clicked me wth device: " + device.getName());
            new Thread() {
                @Override
                public void run() {
                    try {
                        if(device instanceof TemperatureHeatingDevice) {
                            TemperatureHeaterSetupFragment.newInstance(((TemperatureHeatingDevice)device)).show(getFragmentManager(), "test");
                        } else if(device instanceof SwitchDevice) {
                            BackendConfig.getInstance(getApplicationContext()).executeDeviceAction(device, "");
                            refreshAsync(device);
                        }
                    } catch (ServiceNotAvailable serviceNotAvailable) {
                        showError(serviceNotAvailable);
                    }
                    analyticsTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Click")
                            .setLabel("ExecuteDeviceAction")
                            .build());
                }
            }.start();
        }
    }

    private void showError(ServiceNotAvailable serviceNotAvailable) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createDialog("Error occured", "Service is not available").show();
            }
        });
    }

    private void refreshAsync(final SmartHomeDevice device) {
        new Thread() {
            @Override
            public void run() {
                if (BackendConfig.getInstance(getApplicationContext()) instanceof PilightConfig) {
                    try {
                        Thread.sleep(1500L);
                    } catch (InterruptedException e) {
                    }
                }
                if(device == null) {
                    new RefreshDeviceListTask().execute();
                } else {
                    new RefreshDeviceTask().execute(device.getName());
                }

            }
        }.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivityForResult(new Intent(this, SettingsActivity.class), ON_SETTING_RESULT);
            analyticsTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Menu")
                    .setLabel("Settings")
                    .build());
            return true;
        } else if (id == R.id.action_license) {
            new EulaDialog(this).justShow();
            analyticsTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Menu")
                    .setLabel("Eula")
                    .build());
            return true;
        } else if (id == R.id.action_privacy_policy) {
            String url = "https://sites.google.com/site/homecompanionprivacypolicy/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
            analyticsTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Menu")
                    .setLabel("Privacy-Policy")
                    .build());
            return true;
        } else if(id == R.id.action_help) {
            Intent intent = new Intent(this, HelpSlides.class);
            startActivity(intent);
            analyticsTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Menu")
                    .setLabel("Help")
                    .build());
            return true;
        } else if(id == R.id.action_recommend_app) {
            onRecommendationClicked();
            analyticsTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Menu")
                    .setLabel("Recommendation")
                    .build());
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void onRecommendationClicked() {
        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                .setCallToActionText(getString(R.string.invitation_cta))
                .build();
        startActivityForResult(intent, REQUEST_INVITE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(ON_SETTING_RESULT == requestCode) {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            analyticsTracker.updateTrackingAllowedSetting(defaultSharedPreferences.getBoolean("pref_key_opt_out_analytics", false));
            BackendConfig.reset();
            googleAPIHelper.publishSettings(defaultSharedPreferences);
            if (listener != null) {
                listener.onRefresh();
            }
            mListView.setAdapter(null);
        }
    }

    private String getVersion() {
        try {
            return getResources().getString(R.string.version_is) + getApplicationContext().getPackageManager()
                    .getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return getResources().getString(R.string.unknown_version);
    }

    private AlertDialog createDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    private void updateDevicesSettings(List<SmartHomeDevice> devices) {
        Config config = BackendConfig.getInstance(getApplicationContext());
        config.updateDeviceSettings(devices);
        if (devices != null) {
            new NotifyWatch().execute(devices.toArray(new SmartHomeDevice[]{}));
        }
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        //Log.v(TAG, "Receiving data! What shall I do with it?");
    }

    private class DeviceAdapter extends ArrayAdapter<SmartHomeDevice> {

        Context context;
        int layoutResourceId;
        SmartHomeDevice data[] = null;
        private View.OnClickListener onFavClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = mListView.getPositionForView((View) v.getParent());
                //Log.v(this.getClass().getName(), "Title clicked, row " + position);
                if (data != null && data.length > 0 && position < data.length) {
                    data[position].setFav(((CheckBox) v).isChecked());
                    updateDevicesSettings(Arrays.asList(data));
                    analyticsTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Update")
                            .setLabel("Fav")
                            .build());
                }
            }
        };

        private View.OnClickListener onVisibilityClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = mListView.getPositionForView((View) v.getParent());
                //Log.v(this.getClass().getName(), "Title clicked, row " + position);
                if (data != null && data.length > 0 && position < data.length) {
                    data[position].setVisible(((CheckBox) v).isChecked());
                    updateDevicesSettings(Arrays.asList(data));
                    analyticsTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Update")
                            .setLabel("Visibility")
                            .build());
                }
            }
        };

        private View.OnClickListener mOnToggleClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = mListView.getPositionForView((View) v.getParent());
                //Log.v(this.getClass().getName(), "Title clicked, row " + position);
                if (data != null && data.length > 0) {
                    onItemClickForSwitch(data[position]);
                }
            }
        };

        public DeviceAdapter(Context context, int layoutResourceId, SmartHomeDevice[] data) {
            super(context, layoutResourceId, data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
        }

        public void refill(List<SmartHomeDevice> devices) {
            data = devices.toArray(new SmartHomeDevice[]{});
            notifyDataSetChanged();
        }

        public void refill(SmartHomeDevice newDevice) {
            int index = 0;
            for (SmartHomeDevice device : data.clone()) {
                if(device.getAliasOrName().equalsIgnoreCase(newDevice.getAliasOrName()) &&
                        device.getLocationForSwitchOrEmptyString().equalsIgnoreCase(newDevice.getLocationForSwitchOrEmptyString())) {
                    data[index] = newDevice;
                }
                index++;
            }
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            DeviceHolder holder = null;

            if (row == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                holder = new DeviceHolder();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder.symbolText = (TextView) row.findViewById(R.id.symbolText);
                holder.symbolText.setOnClickListener(mOnToggleClickListener);
                holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);
                holder.txtRoom = (TextView) row.findViewById(R.id.txtRoom);
                holder.favIcon = (CheckBox) row.findViewById(R.id.favIcon);
                holder.favIcon.setOnClickListener(onFavClickListener);
                holder.visibilityIcon = (CheckBox) row.findViewById(R.id.visibilityIcon);
                holder.visibilityIcon.setOnClickListener(onVisibilityClickListener);

                row.setTag(holder);
            } else {
                holder = (DeviceHolder) row.getTag();
            }
            if (data != null && data.length > position) {
                SmartHomeDevice device = data[position];
                holder.symbolText.setText("");
                if (device != null) {
                    holder.txtTitle.setText(device.getAliasOrName());
                    holder.txtRoom.setText(device.getLocationsAsString());
                    device = BackendConfig.getInstance(getApplicationContext()).syncDeviceWithSettings(device);
                    holder.visibilityIcon.setChecked(device.isVisible());
                    holder.favIcon.setChecked(device.isFav());

                    if (device instanceof SwitchDevice) {
                        holder.symbolText.setVisibility(View.VISIBLE);
                        holder.symbolText.setText("");
                        if ("on".equalsIgnoreCase(device.getState())) {
                            holder.symbolText.setBackgroundResource(R.drawable.on);
                        } else {
                            holder.symbolText.setBackgroundResource(R.drawable.off);
                        }
                    } else if (device instanceof TemperatureDevice) {
                        holder.txtTitle.setText(device.getAliasOrName());

                        TemperatureDevice tempDevice = ((TemperatureDevice) device);
                        String temperatureFormatted = tempDevice.getTemperatureFormatted();
                        holder.symbolText.setText(temperatureFormatted + TemperatureUtil.DEGREE_SIGN);
                        holder.symbolText.setBackgroundResource(R.drawable.temperature_rounded_bg);
                        TemperatureUtil.configureTemperatureColor(getResources(), holder.symbolText, temperatureFormatted);
                    } else if (device instanceof TemperatureHeatingDevice) {
                        holder.txtTitle.setText(device.getAliasOrName());
                        TemperatureHeatingDevice tempDevice = ((TemperatureHeatingDevice) device);
                        String measuredTemperature = tempDevice.getMeasuredTemperature();
                        holder.symbolText.setText(measuredTemperature + TemperatureUtil.DEGREE_SIGN);
                        holder.symbolText.setBackgroundResource(R.drawable.temperature_rounded_bg);
                        TemperatureUtil.configureTemperatureColor(getResources(), holder.symbolText, measuredTemperature);
                    } else if(device instanceof RelayDevice) {
                        holder.txtTitle.setText(device.getAliasOrName());

                        RelayDevice relayDevice = (RelayDevice) device;
                        holder.symbolText.setText(relayDevice.getState());
                        if("open".equalsIgnoreCase(relayDevice.getState())) {
                            holder.symbolText.setBackgroundResource(R.drawable.open);
                        } else {
                            holder.symbolText.setBackgroundResource(R.drawable.close);
                        }
                    } else {
                        holder.symbolText.setText("?");
                        holder.symbolText.setTextColor(getResources().getColor(de.stefanheintz.smarthome.dto.R.color.white));
                        GradientDrawable background = (GradientDrawable) holder.symbolText.getBackground();
                        background.setColor(getResources().getColor(R.color.wl_gray));
                    }
                }
            }
            return row;
        }

        class DeviceHolder {
            TextView txtTitle;
            TextView txtRoom;
            CheckBox favIcon;
            CheckBox visibilityIcon;
            TextView symbolText;
        }
    }

    private class RefreshDeviceListTask extends AsyncTask<String, Void, List<SmartHomeDevice>> {

        Throwable error = null;

        @Override
        protected void onProgressUpdate(Void... values) {
        }

        @Override
        protected void onPostExecute(List<SmartHomeDevice> retrievedDevices) {
            updateList(error, retrievedDevices);
        }

        @Override
        protected List<SmartHomeDevice> doInBackground(String... params) {
            startTime = System.currentTimeMillis();
            List<SmartHomeDevice> result = new ArrayList<>();
            try {
                result = BackendConfig.getInstance(getApplicationContext()).getDevices();
            } catch (final ServiceNotAvailable serviceNotAvailable) {
                this.error = serviceNotAvailable;
            }
            return result;
        }
    }

    private class RefreshDeviceTask extends AsyncTask<String, Void, SmartHomeDevice> {

        Throwable error = null;

        @Override
        protected void onProgressUpdate(Void... values) {
        }

        @Override
        protected void onPostExecute(SmartHomeDevice retrievedDevice) {
            updateList(error, retrievedDevice);
        }

        @Override
        protected SmartHomeDevice doInBackground(String... params) {
            startTime = System.currentTimeMillis();
            SmartHomeDevice result = null;
            try {
                result = BackendConfig.getInstance(getApplicationContext()).getDevice(params[0]);
            } catch (final ServiceNotAvailable serviceNotAvailable) {
                this.error = serviceNotAvailable;
            }
            return result;
        }
    }

    public class NotifyWatch extends AsyncTask<SmartHomeDevice, Void, Void> {

        @Override
        protected Void doInBackground(SmartHomeDevice... devices) {
            //Log.d("NotifyWatch", "sending data to watch after FAV change");
            if (devices != null) {
                googleAPIHelper.publishList(Arrays.asList(devices), PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));
            }
            return null;
        }
    }
}
