package de.stefanheintz.smarthome;

import android.content.Context;
import android.content.SharedPreferences;

import junit.framework.Assert;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowPreferenceManager;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.stefanheintz.smarthome.config.AbstractConfig;
import de.stefanheintz.smarthome.config.BackendConfig;
import de.stefanheintz.smarthome.config.Credential;
import de.stefanheintz.smarthome.config.HttpCommunicationGateway;
import de.stefanheintz.smarthome.config.RawConfig;
import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;
import de.stefanheintz.smarthome.fhem.FhemConfig;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
@RunWith(RobolectricTestRunner.class)
public class FhemConfigTest extends AbstractConfigTest {

    @Before
    public void before() {
        ClassLoader classLoader = getClass().getClassLoader();
        String fileName = "fhem-demo.config";
        File file = new File(classLoader.getResource(fileName).getFile());
        CONFIG_DIR = file.getPath().replace(fileName, "");
    }

    @Test
    public void test_parse_user_config_tuerkontakt() throws Exception {
        MyTestFhemConfig c = new MyTestFhemConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readFHEMUserConfigThomasTuerkontakt());
        showDevices(smartHomeDevices);
        assertEquals(1, smartHomeDevices.size());
        RelayDevice device = (RelayDevice) smartHomeDevices.get(0);
        Assert.assertEquals("2015-09-08 22:18:09", device.getPreviousTime());
        Assert.assertEquals("Closed", device.getPreviousState());
        Assert.assertEquals("2015-09-08 22:23:22", device.getStateTime());
        Assert.assertEquals("Open", device.getState());
    }


    @Test
    public void test_parse_user_config_successful() throws Exception {
        MyTestFhemConfig c = new MyTestFhemConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readFHEMUserConfig());
        showDevices(smartHomeDevices);
        assertEquals(14, smartHomeDevices.size());
    }

    @Test
    public void test_parse_user_config_holger_successful() throws Exception {
        MyTestFhemConfig c = new MyTestFhemConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readFHEMUserHolgerConfig());
        showDevices(smartHomeDevices);
        assertEquals(55, smartHomeDevices.size());
    }

    @Test
    public void test_parse_user_config_alot_actors_successful() throws Exception {
        MyTestFhemConfig c = new MyTestFhemConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readFHEMUserConfigAlotAktors());
        showDevices(smartHomeDevices);
        assertEquals(24, smartHomeDevices.size());
    }

    private void showDevices(List<SmartHomeDevice> smartHomeDevices) {
        for (SmartHomeDevice smartHomeDevice : smartHomeDevices) {
            System.out.println(smartHomeDevice.getAlias() + " - " + smartHomeDevice.getName() + ", " + smartHomeDevice.getState()
                    + ", " + smartHomeDevice.getClass());
            if(smartHomeDevice instanceof TemperatureHeatingDevice) {
                TemperatureHeatingDevice device = (TemperatureHeatingDevice) smartHomeDevice;
                System.out.println("Desired: " + device.getDesiredTemperature() + ", Measured: " + device.getMeasuredTemperature());
            }
        }
    }

    @Test
    public void test_parse_user_config_with_empty_element_successful() throws Exception {
        MyTestFhemConfig c = new MyTestFhemConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readConfigFile(new File(CONFIG_DIR + "/fhem-user-config-with-empty-element.json")));
        assertEquals(14, smartHomeDevices.size());
    }

    @Test
    public void test_successful_user_config_switch() throws ServiceNotAvailable {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "fhem").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return readFHEMUserConfig().getItemsConfig();
            }
        });
        config.executeDeviceAction("Ventilator");
    }

    @Test
    public void test_successful_fhem_config_reading() throws Exception {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "fhem").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return readFHEMExampleConfig().getItemsConfig();
            }
        });

        List<SmartHomeDevice> devices = config.getDevices();
        assertExpectedDevices(devices);
    }

    @Test
    public void test_successful_fhem_config_tempsensor_reading() throws Exception {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "fhem").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return readFHEMMyTempsensorConfig().getItemsConfig();
            }
        });

        List<SmartHomeDevice> devices = config.getDevices();
        for (SmartHomeDevice device : devices) {
            System.out.println(device.getName() + ", " + device.getState());
            if (device.getAliasOrName().equalsIgnoreCase("TempSensor")) {
                Assert.assertTrue(device instanceof TemperatureDevice);
                Assert.assertEquals("23.8", ((TemperatureDevice) device).getTemperature());
                Assert.assertEquals("41", ((TemperatureDevice) device).getHumidity());
            }
        }
        Assert.assertEquals(6, devices.size());
    }

    @Test
    public void test_successful_fhem_demo_config_reading() throws Exception {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "fhem").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return readFHEMDemoConfig().getItemsConfig();
            }
        });
        List<SmartHomeDevice> devices = config.getDevices();
        showDevices(devices);
        assertEquals(11, devices.size());
    }

    @Test
    public void test_successful_fhem_config_homematic_wandthermostat() throws Exception {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "fhem").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return readFHEM_Homematic_Wandthermostat_Config().getItemsConfig();
            }
        });

        List<SmartHomeDevice> devices = config.getDevices();
        Assert.assertEquals(1, devices.size());
        SmartHomeDevice device = devices.get(0);
        Assert.assertTrue(device instanceof TemperatureHeatingDevice);
        Assert.assertEquals("23.1", ((TemperatureHeatingDevice) device).getMeasuredTemperature());
        Assert.assertEquals("12.0", ((TemperatureHeatingDevice) device).getDesiredTemperature());

    }

    private RawConfig readFHEMDemoConfig() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem-demo.config"));
    }

    private RawConfig readFHEM_Homematic_Wandthermostat_Config() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem-user-config-homematic-wandthermostat.json"));
    }

    private RawConfig readFHEMMyTempsensorConfig() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem-tempsensor.json"));
    }

    private RawConfig readFHEMUserConfig() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem-user-config.json"));
    }

    private RawConfig readFHEMUserConfigThomasTuerkontakt() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem_tuerkontakt.json"));
    }

    private RawConfig readFHEMUserConfigAlotAktors() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem-user-config-alot-actors.json"));
    }

    private RawConfig readFHEMExampleConfig() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem-my.config"));
    }

    private RawConfig readFHEMUserHolgerConfig() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem-user-holger.json"));
    }

    private RawConfig readFHEMUserConfigWithPossibleSets() {
        return readConfigFile(new File(CONFIG_DIR + "/fhem-with-different-on-off-PossibleSets.json"));
    }

    private void assertExpectedDevices(List<SmartHomeDevice> devices) {
        List<SmartHomeDevice> expectedDevices = getDeviceToTestFor();
        assertEquals("not the expected amount of devices found in fhem config", expectedDevices.size(), devices.size());

        for (SmartHomeDevice device : devices) {
            for (SmartHomeDevice expectedDevice : (List<SmartHomeDevice>) ((ArrayList) expectedDevices).clone()) {
                if (expectedDevice.equals(device)) {
                    expectedDevices.remove(expectedDevice);
                }
            }
        }
        StringBuffer message = new StringBuffer();
        for (SmartHomeDevice expectedDevice : expectedDevices) {
            message.append(expectedDevice.getName());
            message.append(",");
            message.append(expectedDevice.getLocationsAsString());
            message.append(",");
            message.append(expectedDevice.getState());
        }
        assertEquals(message.toString(), expectedDevices.size(), 0);
    }

    private List<SmartHomeDevice> getDeviceToTestFor() {
        List<SmartHomeDevice> expectedDevices = new ArrayList<>();
        expectedDevices.add(new SwitchDevice("Regal", newSmartHomeLocation("Unsorted"), "on"));
        expectedDevices.add(new SwitchDevice("PC_Hauptschalter1", newSmartHomeLocation("Unsorted"), "off"));
        expectedDevices.add(new SwitchDevice("Computer", newSmartHomeLocation("Unsorted"), "on"));
        expectedDevices.add(new SwitchDevice("Entertainment", newSmartHomeLocation("Unsorted"), "off"));
        expectedDevices.add(new SwitchDevice("Stehlampe", newSmartHomeLocation("Unsorted"), "off"));
        return expectedDevices;
    }

    private List<SmartHomeDevice> getUserConfigSchlafzimmerDevicesToTestFor() {
        List<SmartHomeDevice> expectedDevices = new ArrayList<>();
        expectedDevices.add(new SwitchDevice("Nachttische", newSmartHomeLocation("1.3 Schlafzimmer"), "on"));
        expectedDevices.add(new SwitchDevice("TVSchlafzimmer", newSmartHomeLocation("1.3 Schlafzimmer"), "on").setAlias("Fernseher"));
        expectedDevices.add(new SwitchDevice("Ladegeraet", newSmartHomeLocation("1.3 Schlafzimmer"), "on").setAlias("Ladegeräte"));
        return expectedDevices;
    }


    @Test
    public void test_successful_switch() throws ServiceNotAvailable {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "fhem").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return "";
            }
        });
        SmartHomeDevice deviceToSwitch = getDeviceToTestFor().get(0);
        config.executeDeviceAction(deviceToSwitch, "");
    }

    @Test
    public void test_grouping_devices_by_room() throws Exception {
        MyTestFhemConfig c = new MyTestFhemConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readFHEMUserConfig());
        assertEquals(14, smartHomeDevices.size());

        Map<SmartHomeLocation, List<SmartHomeDevice>> locationListMap = c.groupByLocation(smartHomeDevices);
        assertEquals(5, locationListMap.size());

        for (SmartHomeLocation smartHomeLocation : locationListMap.keySet()) {
            List<SmartHomeDevice> devices = locationListMap.get(smartHomeLocation);
            if (smartHomeLocation.getName().contains("Schlafzimmer")) {
                for (SmartHomeDevice device : devices) {
                    assertTrue("could not find " + device.getName() +" for location " + device.getLocationsAsString(), findInList(device, getUserConfigSchlafzimmerDevicesToTestFor()));
                }
            }
        }
    }

    @Test
    public void test_grouping_devices_with_no_room() throws Exception {
        MyTestFhemConfig c = new MyTestFhemConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readFHEMUserConfig());
        assertEquals(14, smartHomeDevices.size());

        for (SmartHomeDevice device : smartHomeDevices) {
            device.setLocations(new ArrayList<SmartHomeLocation>());
        }

        Map<SmartHomeLocation, List<SmartHomeDevice>> locationListMap = c.groupByLocation(smartHomeDevices);
        assertEquals(1, locationListMap.size());
        SmartHomeLocation room = (SmartHomeLocation) locationListMap.keySet().toArray()[0];
        assertEquals(SmartHomeLocation.ROOM_NAME_ALL, room.getName());
    }

    private boolean findInList(SmartHomeDevice device, List<SmartHomeDevice> userConfigSchlafzimmerDevicesToTestFor) {
        for (SmartHomeDevice smartHomeDevice : userConfigSchlafzimmerDevicesToTestFor) {
            if (smartHomeDevice.getName().equals(device.getName()) && smartHomeDevice.getLocationsAsString().equals(device.getLocationsAsString())) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void test_parse_user_config_with_PossibleSets_successful() throws Exception {
        MyTestFhemConfig c = new MyTestFhemConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readFHEMUserConfigWithPossibleSets());
        assertEquals(6, smartHomeDevices.size());
    }

    @Test
    public void test_regex() {
        ArrayList<String> testSetTrue = new ArrayList() {{
            add("clear:readings,trigger,register,rssi,msgEvents,all getConfig getRegRaw getSerial getVersion inhibit:on,off off on on-for-timer on-till pair peerBulk peerIODev press raw regBulk regSet reset sign:on,off statusRequest toggle unpair");
            add("Abwesend Anwesend clear:readings,trigger,register,rssi,msgEvents,all getConfig getRegRaw getSerial getVersion inhibit:on,off off on on-for-timer on-till pair peerBulk peerIODev press raw regBulk regSet reset sign:on,off statusRequest toggle unpair");
            add("on off");
            add("on off ");
            add(" on off ");
            add(" off on ");
            add("off on");
            add("off off-for-timer on on-for-timer on-till reset timer toggle blink intervals off-till");
            add("dim06% dim100% dim12% dim18% dim25% dim31% dim37% dim43% dim50% dim56% dim62% dim68% dim75% dim81% dim87% dim93% dimdown dimup dimupdown off off-for-timer on on-100-for-timer-prev on-for-timer on-old-for-timer on-old-for-timer-prev ramp-off-time ramp-on-time reset sendstate timer toggle dim:slider,0,6.25,100 blink on-till intervals off-till");
            add("off:noArg on:noArg  blink toggle on-for-timer on-till off-for-timer intervals off-till");
            add("blink dim06% dim100% dim12% dim18% dim25% dim31% dim37% dim43% dim50% dim56% dim62% dim68% dim75% dim81% dim87% dim93% dim:slider,0,6.25,100 dimdown dimup dimupdown intervals off off-for-timer off-till on on-100-for-timer-prev on-for-timer on-old-for-timer on-old-for-timer-prev on-till ramp-off-time ramp-on-time reset sendstate timer toggle");
            add("dim0% dim100% dim06% dim100% dim12% dim18% dim25% dim31% dim37% dim43% dim50% dim56% dim62% dim68% dim75% dim81% dim87% dim93% dimdown dimup dimupdown off off-for-timer on on-100-for-timer-prev on-for-timer on-old-for-timer on-old-for-timer-prev on-till ramp-off-time ramp-on-time reset sendstate timer toggle dim:slider,0,6.25,100 blink intervals off-till");
            add("on:noArg off:noArg");
            add("runter hoch off:noArg on:noArg blink toggle on-for-timer on-till off-for-timer intervals off-till");
            add("off:noArg on:noArg  blink toggle on-for-timer on-till off-for-timer intervals off-till");
        }};

        ArrayList<String> testSetFalse = new ArrayList() {{
            add("clear:readings,trigger,register,rssi,msgEvents,all getConfig getRegRaw peerBulk peerChan raw regBulk regSet reset sign:on,off unpair");
            add("burstXmit clear:readings,trigger,register,rssi,msgEvents,all controlMode:auto,manual,central,party desired-temp:on,off,6.0,6.5,7.0,7.5,8.0,8.5,9.0,9.5,10.0,10.5,11.0,11.5,12.0,12.5,13.0,13.5,14.0,14.5,15.0,15.5,16.0,16.5,17.0,17.5,18.0,18.5,19.0,19.5,20.0,20.5,21.0,21.5,22.0,22.5,23.0,23.5,24.0,24.5,25.0,25.5,26.0,26.5,27.0,27.5,28.0,28.5,29.0,29.5,30.0 displayMode:temp-only,temp-hum displayTemp:actual,setpoint displayTempUnit:celsius,fahrenheit getConfig getRegRaw partyMode peerBulk peerChan regBulk regSet sign:on,off statusRequest sysTime tempListFri tempListMon tempListSat tempListSun tempListThu tempListTmpl tempListTue tempListWed");
            add("burstXmit clear:readings,trigger,register,rssi,msgEvents,all controlManu:on,off,5.0,5.5,6.0,6.5,7.0,7.5,8.0,8.5,9.0,9.5,10.0,10.5,11.0,11.5,12.0,12.5,13.0,13.5,14.0,14.5,15.0,15.5,16.0,16.5,17.0,17.5,18.0,18.5,19.0,19.5,20.0,20.5,21.0,21.5,22.0,22.5,23.0,23.5,24.0,24.5,25.0,25.5,26.0,26.5,27.0,27.5,28.0,28.5,29.0,29.5,30.0 controlMode:auto,manual,boost,day,night controlParty desired-temp:on,off,5.0,5.5,6.0,6.5,7.0,7.5,8.0,8.5,9.0,9.5,10.0,10.5,11.0,11.5,12.0,12.5,13.0,13.5,14.0,14.5,15.0,15.5,16.0,16.5,17.0,17.5,18.0,18.5,19.0,19.5,20.0,20.5,21.0,21.5,22.0,22.5,23.0,23.5,24.0,24.5,25.0,25.5,26.0,26.5,27.0,27.5,28.0,28.5,29.0,29.5,30.0 getConfig getRegRaw inhibit:on,off peerBulk regBulk regSet sign:on,off sysTime tempListFri tempListMon tempListSat tempListSun tempListThu tempListTmpl tempListTue tempListWed\n");
        }};

        Pattern p = FhemConfig.ON_OFF_PATTERN;
        for (String s : testSetTrue) {
            Matcher matcher = p.matcher(s);
            System.out.println(s);
            assertTrue(matcher.find());
        }

        for (String s : testSetFalse) {
            Matcher matcher = p.matcher(s);
            System.out.println(s);
            assertFalse(matcher.find());
        }

    }

    @Test
    public void test_retrieve_a_single_device_successful() throws ServiceNotAvailable {
        SharedPreferences sharedPreferences = ShadowPreferenceManager.getDefaultSharedPreferences(Robolectric.application.getApplicationContext());
        sharedPreferences.edit().putString("system_preference", "fhem").commit();
        config = BackendConfig.getInstance(Robolectric.application.getApplicationContext());
        ((AbstractConfig) config).setHttpCommunicationGateway(new HttpCommunicationGateway() {
            @Override
            public String httpGet(String url, Credential credential) throws ServiceNotAvailable {
                return readConfigFile(new File(CONFIG_DIR + "/fhem-single-device-ventilator.json")).getItemsConfig();
            }
        });

        SmartHomeDevice ventilator = config.getDevice("Ventilator");
        Assert.assertTrue(ventilator instanceof SwitchDevice);
    }

    private class MyTestFhemConfig extends FhemConfig {
        public MyTestFhemConfig(Context context) {
            super(context);
        }

        @Override
        protected List<SmartHomeDevice> parseConfig(RawConfig rawConfig) throws JSONException {
            return super.parseConfig(rawConfig);
        }

        @Override
        protected Map<SmartHomeLocation, List<SmartHomeDevice>> groupByLocation(List<SmartHomeDevice> deviceList) {
            return super.groupByLocation(deviceList);
        }
    }
}
