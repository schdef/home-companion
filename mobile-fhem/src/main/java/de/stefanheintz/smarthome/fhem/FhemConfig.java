package de.stefanheintz.smarthome.fhem;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import de.stefanheintz.smarthome.ServiceNotAvailable;
import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.config.AbstractConfig;
import de.stefanheintz.smarthome.config.RawConfig;
import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class FhemConfig extends AbstractConfig {

    private static final String DEFAULT_HOST = "192.168.178.33";
    private static final String DEFAULT_PORT = "8083";
    public static final Pattern ON_OFF_PATTERN = Pattern.compile("(on off)|(off on)|(off .* on )|(on .* off )|(off:noArg .* on:noArg)|(on:noArg .* off:noArg)|(off:noArg on:noArg)|(on:noArg off:noArg)");

    public FhemConfig(Context context) {
        super(context, DEFAULT_HOST, DEFAULT_PORT);
    }

    @Override
    public String getImplementationName() {
        return "fhem";
    }

    @Override
    public SmartHomeDevice getDevice(String deviceId) throws ServiceNotAvailable {
        try {
            String command = URLEncoder.encode("jsonlist2 " + deviceId, "UTF-8").replace("+", "%20");
            String url = "http://" + HOST + ":" + PORT + "/fhem?XHR=1&cmd=" + command;

            return getDeviceFromBackend(url);
        } catch (UnsupportedEncodingException e) {
            throw new ServiceNotAvailable("Wrong configuration found. Detail: " + e.getMessage());
        }
    }

    @Override
    public List<SmartHomeDevice> getDevices() throws ServiceNotAvailable {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String roomFilter = sharedPreferences.getString(Settings.PREF_KEY_ROOM_FILTER, null);
        String hostPort = "http://" + HOST + ":" + PORT;
        String command = "/fhem?XHR=1&cmd=jsonlist2";

        if (roomFilter != null) {
            try {
                String encodedCommand = URLEncoder.encode(" room=" + roomFilter, "UTF-8").replace("+", "%20");
                command += encodedCommand;
            } catch (UnsupportedEncodingException e) {
            }
        }

        return getDevices(hostPort + command);
        //return getDevices(("http://192.168.178.42:8100/fhem-test.json"));
        //return getDevices(("http://192.168.178.33:8083/fhem/docs/fhem-marcus-180715.json"));
    }

    @Override
    public void executeDeviceAction(SmartHomeDevice device, String wearCommand) throws ServiceNotAvailable {
        //http://192.168.178.33:8083/fhem?XHR=1&cmd.Regal=set%20Regal%20off&room=Unsorted
        if (device.getLocations() == null) {
            throw new ServiceNotAvailable("Wrong configuration found. Detail: missing device location.");
        }
        String commandUrl = buildActionCommandUrl(device, wearCommand);
        logDebug("FHEM URL: " + commandUrl);
        httpGetRequest(commandUrl);
    }

    private String buildActionCommandUrl(SmartHomeDevice device, String wearCommand) throws ServiceNotAvailable {
        String url = null;
        if(device instanceof SwitchDevice) {
            try {
                String encodedLocation = URLEncoder.encode(device.getLocationForSwitchOrEmptyString(), "UTF-8").replace("+", "%20");
                String command = URLEncoder.encode("set " + device.getName() + " " + inverseState(device.getState()), "UTF-8").replace("+", "%20");
                url = "http://" + HOST + ":" + PORT + "/fhem?XHR=1&cmd=" + command + "&room=" + encodedLocation;
                //            System.out.println("URL: " + url);
            } catch (UnsupportedEncodingException e) {
                throw new ServiceNotAvailable("Wrong configuration found. Detail: " + e.getMessage());
            }
        } else if(device instanceof TemperatureHeatingDevice) {
            try {
                TemperatureHeatingDevice tDevice = (TemperatureHeatingDevice) device;
                String encodedLocation = URLEncoder.encode(device.getLocationForSwitchOrEmptyString(), "UTF-8").replace("+", "%20");
                String command = URLEncoder.encode("set " + tDevice.getName() + " desired-temp "+ wearCommand, "UTF-8").replace("+", "%20");
                url = "http://" + HOST + ":" + PORT + "/fhem?XHR=1&cmd=" + command + "&room=" + encodedLocation;
                //System.out.println("URL: " + url);

            } catch (UnsupportedEncodingException e) {
                throw new ServiceNotAvailable("Wrong configuration found. Detail: " + e.getMessage());
            }
        }
        return url;
    }

    @Override
    protected List<SmartHomeDevice> parseConfig(RawConfig rawConfig) throws JSONException {
        List<SmartHomeDevice> result = new ArrayList<>();
        JSONObject config = new JSONObject(rawConfig.getItemsConfig());
        Iterator<?> jsonKeys = config.keys();
        while (jsonKeys.hasNext()) {
            String element = (String) jsonKeys.next();
            //System.out.println("Element: " + element);
            if ("results".equalsIgnoreCase(element)) {
                JSONArray jsonArray = config.getJSONArray(element);
                for (int i = 0; i < jsonArray.length(); i++) {

                    if (!jsonArray.isNull(i)) {
                        JSONObject resultElement = (JSONObject) jsonArray.get(i);
                        String name = (String) resultElement.get("Name");

                        if (isSupportedDevice(resultElement)) {
                            //System.out.println("Result-Element: " + resultElement.toString());
                            //System.out.println("Result-Element-Name: " + name);
                            JSONObject readings = get(resultElement, "Readings");
                            JSONObject internals = get(resultElement, "Internals");
                            String state = "off";
                            String room = "Unsorted";
                            Iterator<String> resultKeys = resultElement.keys();
                            String alias = null;
                            while (resultKeys.hasNext()) {
                                String resultKey = resultKeys.next();

                                if ("Attributes".equalsIgnoreCase(resultKey)) {
                                    JSONObject attributes = resultElement.getJSONObject("Attributes");
                                    Iterator<String> attributesKeys = attributes.keys();
                                    while (attributesKeys.hasNext()) {
                                        String attributeKey = attributesKeys.next();
                                        if (attributeKey.equalsIgnoreCase("room")) {
                                            room = attributes.getString(attributeKey);
                                        } else if (attributeKey.equalsIgnoreCase("alias")) {
                                            alias = attributes.getString(attributeKey);
                                        }
                                    }
                                }
                            }

                            String typeValue = null;
                            String temperatureValue = null;
                            String humidityValue = null;
                            String measuredTemperatureValue = null;
                            String desiredTemperatureValue = null;

                            if(internals != null) {
                                typeValue = internals.getString("TYPE");
                            }


                            if (readings != null) {
                                JSONObject stateJson = get(readings, "state");
                                if (stateJson != null && "on".equalsIgnoreCase(stateJson.getString("Value"))) {
                                    state = "on";
                                }

                                JSONObject temperature = get(readings, "temperature");
                                if (temperature != null) {
                                    temperatureValue = temperature.getString("Value");
                                }
                                JSONObject measuredTemperature = get(readings, "measured-temp");
                                if (measuredTemperature != null) {
                                    measuredTemperatureValue = measuredTemperature.getString("Value");
                                }

                                JSONObject desiredTemperature = get(readings, "desired-temp");
                                if (desiredTemperature != null) {
                                    desiredTemperatureValue = desiredTemperature.getString("Value");
                                }

                                JSONObject humidity = get(readings, "humidity");
                                if (humidity != null) {
                                    humidityValue = humidity.getString("Value");

                                }
                            }

                            if (name != null && room != null && state != null) {
                                SmartHomeDevice device;
                                if (measuredTemperatureValue != null && desiredTemperatureValue != null && (typeValue != null && ("FHT".equals(typeValue) || "CUL_HM".equals(typeValue)))) {
                                    device = new TemperatureHeatingDevice(name, parseRoomString(room), state);
                                    ((TemperatureHeatingDevice) device).setMeasuredTemperature(measuredTemperatureValue);
                                    ((TemperatureHeatingDevice) device).setDesiredTemperature(desiredTemperatureValue);
                                } else if (temperatureValue != null || measuredTemperatureValue != null) {
                                    device = new TemperatureDevice(name, parseRoomString(room), state);
                                    if (temperatureValue != null) {
                                        ((TemperatureDevice) device).setTemperature(temperatureValue);
                                    } else {
                                        ((TemperatureDevice) device).setTemperature(measuredTemperatureValue);
                                    }
                                    ((TemperatureDevice) device).setHumidity(humidityValue);
                                } else if(typeValue != null && "CUL_FHTTK".equals(typeValue)) {
                                    String stateTime = null;
                                    JSONObject stateJson = get(readings, "state");
                                    if (stateJson != null) {
                                        state = stateJson.getString("Value");
                                        stateTime = stateJson.getString("Time");
                                    }
                                    device = new RelayDevice(name, parseRoomString(room), state);
                                    ((RelayDevice)device).setStateTime(stateTime);
                                    JSONObject previousJson = get(readings, "Previous");
                                    if(previousJson != null) {
                                        String previousTime = previousJson.getString("Time");
                                        ((RelayDevice)device).setPreviousTime(previousTime);
                                        String previousState = previousJson.getString("Value");
                                        ((RelayDevice)device).setPreviousState(previousState);
                                    }
                                } else {
                                    device = new SwitchDevice(name, parseRoomString(room), state);
                                }

                                device.setAlias(alias);
                                result.add(device);

                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private boolean isSupportedDevice(JSONObject element) throws JSONException {
        String possibleSets = (String) element.get("PossibleSets");
        if ((possibleSets != null && (containsOnOff(possibleSets))) || isTemperatureElement(element)) {
            return true;
        }
        return false;
    }

    protected boolean isTemperatureElement(JSONObject element) throws JSONException {
        JSONObject readings = get(element, "Readings");
        JSONObject internals = get(element, "Internals");
        if (readings != null) {
            JSONObject temperature = get(readings, "temperature");
            if (temperature != null) {
                return true;
            } else {
                // Homematic_Wandthermostate
                temperature = get(readings, "measured-temp");
                if (temperature != null) {
                    return true;
                }
            }
        }
        if(internals != null) {
            String typeValue = internals.getString("TYPE");
            if (typeValue != null && ("FHT".equals(typeValue) || "CUL_FHTTK".equals(typeValue))) {
                return true;
            }
        }
        return false;
    }

    private JSONObject get(JSONObject element, String searchedElement) throws JSONException {
        Iterator<String> elementKeys = element.keys();
        while (elementKeys.hasNext()) {
            String elementKey = elementKeys.next();
            if (searchedElement.equalsIgnoreCase(elementKey)) {
                return element.getJSONObject(elementKey);
            }
        }
        return null;
    }

    protected boolean containsOnOff(String possibleSets) {
        return ON_OFF_PATTERN.matcher(possibleSets).find();
    }
}