Travis Build Status https://travis-ci.org/schdef/homecompanion
# Home Companion for Wear
An Android Wear application to control your smart home. See google play store https://play.google.com/store/apps/details?id=de.stefanheintz.smarthome to download.

![feature-graphic.png](http://4.bp.blogspot.com/-ddBNCbpE0HA/ViOcqcEEzJI/AAAAAAAAk7s/epD0cEcpK0E/s1600/feature-graphic.png)

## Architecture
![homecompanion-overview.png](https://bytebucket.org/schdef/home-companion/raw/48d3b4dcd2f9783b1989b9c5910f6ef71bed4fbf/resources/homecompanion-overview.png?token=088334eb78404dd0303b820769eb254936d0e9b1)

## Functionality
* status of all switchable devices
* switch devices on and off
* open/closed status of sensors
* read temperature of sensors
* set temperature of heater controls

## Supporting systems
* [pilight.org](http://pilight.org)
* [fhem.de](http://fhem.de)
* [openHAB.org](http://openHAB.org)

## Development
Feel free to write issues, enhancements or questions to the [Issues](https://bitbucket.org/schdef/home-companion/issues?status=new&status=open) section.