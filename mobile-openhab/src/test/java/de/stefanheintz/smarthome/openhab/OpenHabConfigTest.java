package de.stefanheintz.smarthome.openhab;

import android.content.Context;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.stefanheintz.smarthome.AbstractConfigTest;
import de.stefanheintz.smarthome.config.RawConfig;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.dto.TemperatureDevice;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * The MIT License (MIT)
 * <p/>
 * Copyright (c) 2015 Stefan Heintz
 * <p/>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p/>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p/>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@RunWith(RobolectricTestRunner.class)
public class OpenHabConfigTest extends AbstractConfigTest {

    @Before
    public void before() {
        ClassLoader classLoader = getClass().getClassLoader();
        String fileName = "openhab-config.json";
        File file = new File(classLoader.getResource(fileName).getFile());
        CONFIG_DIR = file.getPath().replace(fileName, "");
    }

    @Test
    public void test_parse_user_config_successful() throws Exception {
        List<String> expectedItems = getExpectedSitemapItemNames();

        MyTestOpenHabConfig c = new MyTestOpenHabConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readDefaultConfig());
        for (SmartHomeDevice smartHomeDevice : smartHomeDevices) {
            System.out.println("Location:" + smartHomeDevice.getLocationsAsString() + ", Alias: " + smartHomeDevice.getAlias() + ", Name: " + smartHomeDevice.getName());
            assertNotNull(smartHomeDevice.getLocationForSwitchOrEmptyString());

            for (String expectedItem : expectedItems) {
                if (expectedItem.equals(smartHomeDevice.getName())) {
                    expectedItems.remove(expectedItem);
                    break;
                }
            }
        }
        Assert.assertEquals(0, expectedItems.size());
        Assert.assertEquals(51, smartHomeDevices.size());

        Map<SmartHomeLocation, List<SmartHomeDevice>> locationListMap = c.groupByLocation(smartHomeDevices);
        assertEquals(14, locationListMap.size());
    }

    @Test
    public void test_parse_default_config_successful() throws Exception {
        MyTestOpenHabConfig c = new MyTestOpenHabConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readDemoConfig());
        for (SmartHomeDevice smartHomeDevice : smartHomeDevices) {
            System.out.println("Location:" + smartHomeDevice.getLocationsAsString() + ", Alias: " + smartHomeDevice.getAlias() + ", Name: " + smartHomeDevice.getName());
            assertNotNull(smartHomeDevice.getLocationForSwitchOrEmptyString());
        }
        Assert.assertEquals(49, smartHomeDevices.size());
    }

    @Test
    public void test_parse_demo_config_successful_ff_bath_temperature() throws Exception {
        MyTestOpenHabConfig c = new MyTestOpenHabConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(readDemoConfig());
        for (SmartHomeDevice smartHomeDevice : smartHomeDevices) {
            System.out.println("Location:" + smartHomeDevice.getLocationsAsString() + ", Alias: " + smartHomeDevice.getAlias() + ", Name: " + smartHomeDevice.getName());
            if("Temperature_FF_Bath".equals(smartHomeDevice.getName())) {
                assertTrue(smartHomeDevice instanceof TemperatureDevice);
                assertEquals("21.6", ((TemperatureDevice)smartHomeDevice).getTemperatureFormatted());
            }
        }
    }

    @Test
    public void test_parse_items_config_successful_issue_11() throws JSONException {
        RawConfig rawConfig = readConfigFile(new File(CONFIG_DIR + "/openhab-sitemap-mobile-items-issue-11.json"));
        rawConfig.setSitemapConfig(readFile(new File(CONFIG_DIR + "/openhab-sitemap-mobile-items-issue-11.json")));

        MyTestOpenHabConfig c = new MyTestOpenHabConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(rawConfig);
        assertNotNull(smartHomeDevices);
        assertEquals(2, smartHomeDevices.size());
    }

    @Test
    public void test_parse_items_config_successful_issue_14() throws JSONException {
        RawConfig rawConfig = readConfigFile(new File(CONFIG_DIR + "/openhab2-sitemap-test-items-issue-14.json"));
        rawConfig.setSitemapConfig(readFile(new File(CONFIG_DIR + "/openhab2-sitemap-test-items-issue-14.json")));

        MyTestOpenHabConfig c = new MyTestOpenHabConfig(Robolectric.application.getApplicationContext());
        List<SmartHomeDevice> smartHomeDevices = c.parseConfig(rawConfig);
        assertNotNull(smartHomeDevices);
        //for (SmartHomeDevice device : smartHomeDevices) {
        //    System.out.println(device.getAliasOrName() +","+device.getLocationsAsString()+","+device.getState());
        //}
        assertEquals(13, smartHomeDevices.size());

    }

    private List<String> getExpectedSitemapItemNames() {
        List<String> expectedItems = new ArrayList<>();

        //Overview
        expectedItems.add("Temperature_Setpoint");
        // Outdoor
        expectedItems.add("Light_Outdoor_Garage");
        expectedItems.add("Light_Outdoor_Terrace");
        expectedItems.add("Light_Outdoor_Frontdoor");
        expectedItems.add("Weather_Temp_Max");
        expectedItems.add("Weather_Temp_Min");
        expectedItems.add("Weather_Humidity");
        expectedItems.add("Weather_Humidex");
        expectedItems.add("Weather_Chart_Period");

        // Cellar
        expectedItems.add("Light_C_Corridor_Ceiling");
        expectedItems.add("Light_C_Staircase");
        expectedItems.add("Light_C_Washing_Ceiling");
        expectedItems.add("Light_C_Workshop");

        // Firstfloor
        expectedItems.add("Light_FF_Bath_Ceiling");
        expectedItems.add("Light_FF_Bath_Mirror");
        expectedItems.add("Heating_FF_Bath");
        expectedItems.add("Light_FF_Office_Ceiling");
        expectedItems.add("Heating_FF_Office");
        expectedItems.add("Light_FF_Child_Ceiling");
        expectedItems.add("Heating_FF_Child");
        expectedItems.add("Light_FF_Bed_Ceiling");
        expectedItems.add("Heating_FF_Bed");
        expectedItems.add("Light_FF_Corridor_Ceiling");
        expectedItems.add("Temperature_FF_Bath");
        expectedItems.add("Temperature_FF_Office");
        expectedItems.add("Temperature_FF_Child");
        expectedItems.add("Temperature_FF_Bed");

        // Groundfloor
        expectedItems.add("Heating_GF_Living");
        expectedItems.add("Light_GF_Kitchen_Ceiling");
        expectedItems.add("Light_GF_Kitchen_Table");
        expectedItems.add("Heating_GF_Kitchen");
        expectedItems.add("Light_GF_Toilet_Ceiling");
        expectedItems.add("Light_GF_Toilet_Mirror");
        expectedItems.add("Heating_GF_Toilet");
        expectedItems.add("Light_GF_Corridor_Ceiling");
        expectedItems.add("Light_GF_Corridor_Wardrobe");
        expectedItems.add("Heating_GF_Corridor");
        expectedItems.add("Temperature_GF_Living");
        expectedItems.add("Temperature_GF_Kitchen");
        expectedItems.add("Temperature_GF_Toilet");

        return expectedItems;
    }

    private RawConfig readDefaultConfig() {
        RawConfig rawConfig = readConfigFile(new File(CONFIG_DIR + "/openhab-sitemap-default-items.json"));
        return rawConfig.setSitemapConfig(readFile(new File(CONFIG_DIR + "/openhab-sitemap-default-items.json")));
    }

    private RawConfig readDemoConfig() {
        RawConfig rawConfig = readConfigFile(new File(CONFIG_DIR + "/openhab-sitemap-demo-items.json"));
        return rawConfig.setSitemapConfig(readFile(new File(CONFIG_DIR + "/openhab-sitemap-demo-items.json")));
    }

    private class MyTestOpenHabConfig extends OpenHabConfig {

        protected MyTestOpenHabConfig(Context context) {
            super(context);
        }

        @Override
        protected List<SmartHomeDevice> parseConfig(RawConfig rawConfig) throws JSONException {
            return super.parseConfig(rawConfig);
        }

        @Override
        protected Map<SmartHomeLocation, List<SmartHomeDevice>> groupByLocation(List<SmartHomeDevice> deviceList) {
            return super.groupByLocation(deviceList);
        }
    }


}
