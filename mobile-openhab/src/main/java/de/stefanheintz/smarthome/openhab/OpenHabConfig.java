package de.stefanheintz.smarthome.openhab;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.stefanheintz.smarthome.ServiceNotAvailable;
import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.config.AbstractConfig;
import de.stefanheintz.smarthome.config.Config;
import de.stefanheintz.smarthome.config.RawConfig;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class OpenHabConfig extends AbstractConfig implements Config {

    private static final String DEFAULT_HOST = "192.168.178.33";
    private static final String DEFAULT_PORT = "8080";
    private List<Sitemap> sitemapList = new ArrayList<>();
    private Widget currentWidgetOfLinkedPage = null;
    private LinkedPage currentLinkedPage;

    public OpenHabConfig(Context context) {
        super(context, DEFAULT_HOST, DEFAULT_PORT);
    }

    @Override
    public String getImplementationName() {
        return "openhab";
    }

    @Override
    protected List<SmartHomeDevice> parseConfig(RawConfig rawConfig) throws JSONException {
        sitemapList = parseSitemapConfig(rawConfig.getSitemapConfig());
        List<SmartHomeDevice> smartHomeDevices = parseItemsConfig(sitemapList);
        return removeDuplicate(smartHomeDevices);
    }

    private List<SmartHomeDevice> removeDuplicate(List<SmartHomeDevice> smartHomeDevices) {
        List<SmartHomeDevice> result = new ArrayList<>();
        for (SmartHomeDevice device : smartHomeDevices) {
            if(!isInList(result, device)) {
                result.add(device);
            }
        }
        return result;
    }

    private boolean isInList(List<SmartHomeDevice> smartDevices, SmartHomeDevice device) {
        for (SmartHomeDevice smartDevice : smartDevices) {
            if(smartDevice.equals(device)) {
                return true;
            }
        }
        return false;
    }

    private List<SmartHomeDevice> parseItemsConfig(List<Sitemap> sitemapList) {
        List<SmartHomeDevice> result = new ArrayList<>();
        for (Sitemap sitemap : sitemapList) {
            List<Widget> widgets = sitemap.widgets;
            for (Widget widget : widgets) {
                List<SmartHomeDevice> item = resolveForType(widget);
                if(item != null) {
                    result.addAll(item);
                } else if ((widget.type.equals("Frame") && widget.label.equals(""))) {
                    result.addAll(resolveForType(widget));
                }
            }
        }

        return result;
    }

    private List<SmartHomeDevice> resolveForType(Widget widget) {
        List<SmartHomeDevice> result = new ArrayList<>();
        if (widget == null) {
            return result;
        }

        if (widget.linkedPage != null) {
            if ("false".equalsIgnoreCase(widget.linkedPage.leaf)) {
                currentLinkedPage = widget.linkedPage;
            }
            List<Widget> linkedPageWidgets = widget.linkedPage.widgets;
            for (Widget widgetOfLinkedPage : linkedPageWidgets) {
                if (currentWidgetOfLinkedPage == null) {
                    currentWidgetOfLinkedPage = widgetOfLinkedPage;
                }
                result.addAll(resolveForType(widgetOfLinkedPage));
            }
            currentWidgetOfLinkedPage = null;
        } else if (widget.widgets.size() > 0) {
            for (Widget widgetWidget : widget.widgets) {
                result.addAll(resolveForType(widgetWidget));
            }
        } else if (widget.item != null) {
            if ("SwitchItem".equals(widget.item.type)) {
                String location = resolveLocation();
                SmartHomeDevice device = new SwitchDevice(widget.item.name, parseRoomString(location), widget.item.state).setAlias(widget.label);
                result.add(device);
            } else if ("NumberItem".equals(widget.item.type)) {
                String location = resolveLocation();
                TemperatureDevice device = new TemperatureDevice(widget.item.name, parseRoomString(location), widget.item.state);
                device.setAlias(widget.label);
                device.setTemperature(widget.item.state);
                result.add(device);
            }
        }
        return result;
    }

    private String resolveLocation() {
        if (currentWidgetOfLinkedPage != null) {
            if (currentLinkedPage != null) {
                return currentLinkedPage.title + " " + currentWidgetOfLinkedPage.label;
            }
            return currentWidgetOfLinkedPage.label;
        }
        return null;
    }

    private List<Sitemap> parseSitemapConfig(String sitemapConfig) throws JSONException {
        List<Sitemap> result = new ArrayList<>();
        if (sitemapConfig != null) {
            JSONObject config = new JSONObject(sitemapConfig);
            Iterator<?> jsonKeys = config.keys();
            while (jsonKeys.hasNext()) {
                String element = (String) jsonKeys.next();
                //System.out.println("Element: " + element);
                if ("homepage".equalsIgnoreCase(element)) {
                    JSONObject homepageElement = config.getJSONObject(element);
                    JSONArray widget = homepageElement.optJSONArray("widget");
                    if(widget != null){
                        result.add(new Sitemap(widget));
                    } else {
                        result.add(new Sitemap(homepageElement.getJSONArray("widgets")));
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List getDevices() throws ServiceNotAvailable {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String systemConfig = sharedPreferences.getString(Settings.PREF_KEY_SYSTEM_CONFIG, "demo");

        return getDevices("http://" + HOST + ":" + PORT + "/rest/sitemaps/"+systemConfig+"?type=json");
    }

    @Override
    public void executeDeviceAction(SmartHomeDevice device, String wearCommand) throws ServiceNotAvailable {
        // http://192.168.178.33:8080/CMD?Light_FF_Bath_Ceiling=TOGGLE&__async=true&__source=waFF_Bath
        if (device.getLocations() == null) {
            throw new ServiceNotAvailable("Wrong configuration found. Detail: missing device location.");
        }
        String url = null;
        try {
            String encodedLocation = URLEncoder.encode(device.getLocationForSwitchOrEmptyString(), "UTF-8").replace("+", "%20");
            String command = URLEncoder.encode("set " + device.getName() + " " + inverseState(device.getState()), "UTF-8").replace("+", "%20");
            url = "http://" + HOST + ":" + PORT + "/CMD?" + device.getName() + "=TOGGLE";
            //System.out.println("URL: " + url);
        } catch (UnsupportedEncodingException e) {
            throw new ServiceNotAvailable("Wrong configuration found. Detail: " + e.getMessage());
        }
        httpGetRequest(url);
    }

    private class Sitemap {

        List<Widget> widgets = new ArrayList<>();

        public Sitemap(JSONArray array) throws JSONException {
            for (int i = 0; i < array.length(); i++) {
                if (!array.isNull(i)) {
                    JSONObject jsonObject = (JSONObject) array.get(i);
                    widgets.add(new Widget(jsonObject));
                }
            }
        }
    }

    private class Widget {
        String icon;
        LinkedPage linkedPage;
        WidgetItem item;
        String widgetId;
        String label;
        String type;
        List<Widget> widgets = new ArrayList<>();

        public Widget(JSONObject jsonObject) throws JSONException {
            JSONArray widgetArray = jsonObject.optJSONArray("widget");
            if(widgetArray == null) {
                widgetArray = jsonObject.optJSONArray("widgets");
            }
            if (widgetArray != null) {
                for (int i = 0; i < widgetArray.length(); i++) {
                    if (!widgetArray.isNull(i)) {
                        JSONObject widgetObject = (JSONObject) widgetArray.get(i);
                        widgets.add(new Widget(widgetObject));
                    }
                }
            } else if (jsonObject.optJSONObject("widget") != null) {
                widgets.add(new Widget(jsonObject.getJSONObject("widget")));
            }
            if (jsonObject.has("icon")) {
                this.icon = jsonObject.getString("icon");
            }
            if (jsonObject.has("widgetId")) {
                this.widgetId = jsonObject.getString("widgetId");
            }
            if (jsonObject.has("label")) {
                this.label = jsonObject.getString("label");
            }
            if (jsonObject.has("type")) {
                this.type = jsonObject.getString("type");
            }
            if (jsonObject.optJSONObject("item") != null) {
                this.item = new WidgetItem(jsonObject.getJSONObject("item"));
            }
            if (jsonObject.optJSONObject("linkedPage") != null) {
                this.linkedPage = new LinkedPage(jsonObject.getJSONObject("linkedPage"));
            }
        }
    }

    private class LinkedPage {
        String id;
        String title;
        String icon;
        String link;
        String leaf;
        List<Widget> widgets = new ArrayList<>();

        public LinkedPage(JSONObject jsonObject) throws JSONException {
            if (jsonObject.has("id")) {
                this.id = jsonObject.getString("id");
            }
            if (jsonObject.has("link")) {
                this.link = jsonObject.getString("link");
            }
            if (jsonObject.has("title")) {
                this.title = jsonObject.getString("title");
            }
            if (jsonObject.has("icon")) {
                this.icon = jsonObject.getString("icon");
            }
            if (jsonObject.has("leaf")) {
                this.leaf = jsonObject.getString("leaf");
            }
            if (jsonObject.optJSONArray("widget") != null || jsonObject.optJSONArray("widgets") != null) {
                JSONArray widgetArray = jsonObject.optJSONArray("widget");
                if(widgetArray == null) {
                    widgetArray = jsonObject.optJSONArray("widgets");
                }
                if(widgetArray != null) {
                    for (int i = 0; i < widgetArray.length(); i++) {
                        if (!widgetArray.isNull(i)) {
                            JSONObject widgetObject = (JSONObject) widgetArray.get(i);
                            widgets.add(new Widget(widgetObject));
                        }
                    }
                }
            } else if (jsonObject.optJSONObject("widget") != null) {
                widgets.add(new Widget(jsonObject.getJSONObject("widget")));
            }
        }
    }

    private class WidgetItem {
        String link;
        String name;
        String state;
        String type;

        public WidgetItem(JSONObject jsonObject) throws JSONException {
            if (jsonObject.has("link")) {
                this.link = jsonObject.getString("link");
            }
            if (jsonObject.has("name")) {
                this.name = jsonObject.getString("name");
            }
            if (jsonObject.has("state")) {
                this.state = jsonObject.getString("state");
            }
            if (jsonObject.has("type")) {
                this.type = jsonObject.getString("type");
            }
        }
    }
}