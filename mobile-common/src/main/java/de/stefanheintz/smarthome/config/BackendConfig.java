package de.stefanheintz.smarthome.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

import de.stefanheintz.smarthome.DummyConfig;


/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class BackendConfig {

    private static Config instance;

    public static void reset() {
        synchronized (BackendConfig.class) {
            instance = null;
        }
    }

    public static Config getInstance(Context context) {
        if (instance == null) {
            synchronized (BackendConfig.class) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                String systemSetting = preferences.getString("system_preference", "pilight");

                String config = getConfig(systemSetting);
                if(config == null) {
                    instance = new DummyConfig(context);
                } else {
                    try {
                        Class<?> configClass = Class.forName(config);
                        Constructor<?> constructor = configClass.getConstructor(Context.class);
                        instance = (Config) constructor.newInstance(context);
                    } catch (Exception e) {
                        throw new RuntimeException("No system configuration found!");
                    }
                }
            }
        }
        return instance;
    }

    private static String getConfig(String config) {
        ArrayList<String> list = new ArrayList() {{
            add("de.stefanheintz.smarthome.fhem.FhemConfig");
            add("de.stefanheintz.smarthome.pilight.PilightConfig");
            add("de.stefanheintz.smarthome.openhab.OpenHabConfig");
        }};

        for (String configItem : list) {
            if(configItem.contains(config.toLowerCase())) {
                return configItem;
            }
        }
        return null;
    }
}
