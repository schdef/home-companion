package de.stefanheintz.smarthome.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.stefanheintz.smarthome.dto.GroupingUtil;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.ServiceNotAvailable;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public abstract class AbstractConfig implements Config {

    public static final String PREF_KEY_DEVICE_SETTINGS = "pref_device_settings";
    protected final Context context;
    protected String HOST;
    protected String PORT;
    protected boolean isEnabledCredentialUsage;
    protected boolean isEnabledSSLUsage;
    protected boolean isDebugMode;
    protected Credential CREDENTIAL;
    private SharedPreferences preferences;
    private HttpCommunicationGateway comGateway = new DefaultHttpCommunicationGateway();
    private List<SmartHomeDevice> deviceSettingList = new ArrayList<>();
    private Gson gson = new GsonBuilder().create();
    private List<SmartHomeDevice> cachedDevices = new ArrayList<>();

    protected AbstractConfig(Context context, String defaultHost, String defaultPort) {
        this.context = context;
        loadProperties(defaultHost, defaultPort);
    }

    protected void loadProperties(String defaultHost, String defaultPort) {
        if (context != null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
            HOST = preferences.getString("pref_key_host", defaultHost);
            PORT = preferences.getString("pref_key_port", defaultPort);
            String username = preferences.getString("pref_key_username", null);
            String password = preferences.getString("pref_key_password", null);
            isEnabledCredentialUsage = preferences.getBoolean("pref_key_en_dis_cred", false);
            if (username != null && password != null && !username.trim().equalsIgnoreCase("") && !password.trim().equalsIgnoreCase("")) {
                CREDENTIAL = new Credential(username, password);
            }
            isEnabledSSLUsage = preferences.getBoolean("pref_key_en_dis_ssl", false);
            isDebugMode = preferences.getBoolean("pref_key_en_dis_debug_mode", false);


            String favoritePrefString = preferences.getString(PREF_KEY_DEVICE_SETTINGS, "");
            //Log.d(this.getImplementationName(), "deviceSettings: " + favoritePrefString);
            if (favoritePrefString != null && favoritePrefString.trim().length() > 0) {
                Type type = new TypeToken<List<SmartHomeDevice>>() {
                }.getType();
                deviceSettingList = gson.fromJson(favoritePrefString, type);
            }
        } else {
            HOST = defaultHost;
            PORT = defaultPort;
            CREDENTIAL = null;
        }
    }

    public SmartHomeDevice getDeviceFromBackend(String url) throws ServiceNotAvailable {
        SmartHomeDevice result = null;
        List<SmartHomeDevice> devices = getDevices(url);
        if(devices != null && devices.size() > 0) {
            result = devices.get(0);
        }
        return result;
    }

    @Override
    public SmartHomeDevice getDevice(String deviceId) throws ServiceNotAvailable {
        // TODO: not implemented yet by all backend systems
        List<SmartHomeDevice> devices = getDevices();
        for (SmartHomeDevice device : devices) {
            if(device.getName().toLowerCase().equalsIgnoreCase(deviceId.toLowerCase())) {
                return device;
            }
        }
        return null;
    }

    public List<SmartHomeDevice> getDevices(String url) throws ServiceNotAvailable {
        List<SmartHomeDevice> result = new ArrayList<>();
        try {
            String configString = getRemoteConfig(url);
            if (configString == null) {
                throw new ServiceNotAvailable("Communication to server successful, but no config found!");
            }
            List<SmartHomeDevice> devices = readConfig(RawConfig.newInstance().setItemsConfig(configString).setSitemapConfig(configString));
            for (SmartHomeDevice device : devices) {
                result.add(syncDeviceWithSettings(device));
            }
        } catch (Throwable t) {
            //Log.e(getImplementationName(), "Error: " + t.getMessage());
            if (t instanceof ServiceNotAvailable) {
                throw t;
            } else {
                throw new ServiceNotAvailable(t.getMessage());
            }
        }
        Collections.sort(result);
        cacheDevices(result);
        return result;
    }

    private void cacheDevices(List<SmartHomeDevice> result) {
        this.cachedDevices = result;
    }

    protected List<SmartHomeDevice> readConfig(RawConfig rawConfig) throws ServiceNotAvailable {
        List<SmartHomeDevice> result = Collections.EMPTY_LIST;
        try {
            result = parseConfig(rawConfig);
        } catch (JSONException e) {
            throw new ServiceNotAvailable("Could not read device list from server! Error is: " + e.getMessage());
            //Log.e(getImplementationName(), e.getMessage());
        }
        //cacheConfig(rawConfig);
        return result;
    }

    protected abstract List<SmartHomeDevice> parseConfig(RawConfig rawConfig) throws JSONException;

    @Override
    public void updateDeviceSettings(List devices) {
        deviceSettingList = devices;
        JsonArray deviceListAsJson = gson.toJsonTree(devices).getAsJsonArray();
        String jsonString = deviceListAsJson.toString();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_KEY_DEVICE_SETTINGS, jsonString);
        editor.commit();
    }

    @Override
    public SmartHomeDevice syncDeviceWithSettings(SmartHomeDevice device) {
        for (SmartHomeDevice settings : deviceSettingList) {
            if (settings != null &&
                    device != null &&
                    settings.getName() != null &&
                    device.getName() != null &&
                    settings.getName().equals(device.getName()) &&
                    settings.getLocationsAsString().equals(device.getLocationsAsString())) {
                device.setFav(settings.isFav());
                device.setVisible(settings.isVisible());
                return device;
            }
        }
        return device;
    }
    private void cacheConfig(String rawConfig) {
        if (rawConfig != null && rawConfig.length() > 0 && context != null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(getImplementationName() + ".config", rawConfig);
            editor.apply();
        }
    }

    protected String loadConfigFromCache() {
        //if (context != null) {
        //SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        //return preferences.getString(getImplementationName() + ".config", null);
        //}
        return null;
    }

    protected String getRemoteConfig(String url) throws ServiceNotAvailable {
        return httpGetRequest(url);
    }

    protected String httpGetRequest(String url) throws ServiceNotAvailable {
        if (isEnabledSSLUsage && url != null) {
            url = url.replace("http:", "https:");
        }
        logDebug("URL: " + url);
        if (isEnabledCredentialUsage) {
            return comGateway.httpGet(url, CREDENTIAL);
        } else {
            return comGateway.httpGet(url, null);
        }
    }

    protected String inverseState(String state) {
        if (state.contains("on")) {
            state = "off";
        } else {
            state = "on";
        }
        return state;
    }

    @Override
    public void executeDeviceAction(String deviceNameIdToSwitch) throws ServiceNotAvailable {
        deviceNameIdToSwitch = deviceNameIdToSwitch.toLowerCase();

        logDebug("looking for device "+deviceNameIdToSwitch);
        List<SmartHomeDevice> devices = getDevices();
        if (devices != null && devices.size() > 0) {
            for (SmartHomeDevice device : devices) {
                String deviceId = (device.getName() + "~" + device.getLocationForSwitchOrEmptyString()).toLowerCase();

                if (deviceNameIdToSwitch.startsWith(deviceId)) {
                    logDebug("found device "+ device.getAliasOrName());
                    int commandIndex = deviceNameIdToSwitch.lastIndexOf("~");
                    String wearCommand = null;
                    if(commandIndex > -1) {
                        wearCommand = deviceNameIdToSwitch.substring(commandIndex+1);
                    }
                    executeDeviceAction(device, wearCommand);
                }
            }
        }
    }

    protected void logDebug(final String message) {
        if(isDebugMode) {
            Handler mHandler = new Handler(context.getMainLooper()) {
                @Override
                public void handleMessage(Message message) {
                    Toast.makeText(context, message.obj.toString(), Toast.LENGTH_LONG).show();
                }
            };
            Message m = mHandler.obtainMessage(0, message);
            m.sendToTarget();
        }
    }

    protected Map<SmartHomeLocation, List<SmartHomeDevice>> groupByLocation(List<SmartHomeDevice> deviceList) {
        return GroupingUtil.groupByLocation(deviceList);
    }

    public void setHttpCommunicationGateway(HttpCommunicationGateway communicationGateway) {
        this.comGateway = communicationGateway;
    }

    protected List<SmartHomeLocation> parseRoomString(String roomString) {
        List<SmartHomeLocation> rooms = new ArrayList<>();
        if(roomString != null) {
            String[] splitRooms = roomString.split(",");
            for (String splitRoom : splitRooms) {
                rooms.add(new SmartHomeLocation(splitRoom.trim()));
            }
        }
        return rooms;
    }

    @Override
    public List getLastRetrievedDevices() {
        return cachedDevices;
    }
}
