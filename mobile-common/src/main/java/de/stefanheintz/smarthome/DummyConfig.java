package de.stefanheintz.smarthome;

import android.content.Context;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import de.stefanheintz.smarthome.config.AbstractConfig;
import de.stefanheintz.smarthome.config.RawConfig;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class DummyConfig extends AbstractConfig {

    public DummyConfig(Context context) {
        super(context, "", "");
    }

    protected DummyConfig(Context context, String defaultHost, String defaultPort) {
        super(context, defaultHost, defaultPort);
    }

    @Override
    public String getImplementationName() {
        return "dummy";
    }

    @Override
    protected List<SmartHomeDevice> parseConfig(RawConfig rawConfig) throws JSONException {
        return new ArrayList<>();
    }

    @Override
    public List getDevices() throws ServiceNotAvailable {
        return new ArrayList<>();
    }

    @Override
    public void executeDeviceAction(SmartHomeDevice device, String wearCommand) throws ServiceNotAvailable {
    }
}
