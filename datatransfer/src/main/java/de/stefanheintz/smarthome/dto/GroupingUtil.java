package de.stefanheintz.smarthome.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class GroupingUtil {

    public static Map<SmartHomeLocation, List<SmartHomeDevice>> groupByLocation(List<SmartHomeDevice> deviceList) {
        Collections.sort(deviceList);
        return groupByLocation(deviceList, true);
    }

    public static Map<SmartHomeLocation, List<SmartHomeDevice>> groupByLocation(List<SmartHomeDevice> deviceList, boolean withFavAndAll) {
        Map<SmartHomeLocation, List<SmartHomeDevice>> map = new TreeMap<>();
        SmartHomeLocation favKey = new SmartHomeLocation(SmartHomeLocation.ROOM_NAME_FAVORITES);
        for (SmartHomeDevice device : deviceList) {
            List<SmartHomeLocation> locations = device.getLocations();
            for (SmartHomeLocation location : locations) {
                if (map.get(location) == null) {
                    map.put(location, new ArrayList<SmartHomeDevice>());
                }
                map.get(location).add(device);
            }
            if (withFavAndAll && device.isFav()) {
                if (map.get(favKey) == null) {
                    map.put(favKey, new ArrayList<SmartHomeDevice>());
                }
                map.get(favKey).add(device);
            }
        }
        if (withFavAndAll) {
            if(map.size() == 0) {
                map.put(new SmartHomeLocation(SmartHomeLocation.ROOM_NAME_ALL), deviceList);
            }
        }

        return map;
    }

    public static List<SmartHomeDevice> sortByRoomAndDeviceName(List<SmartHomeDevice> deviceList) {
        Map<SmartHomeLocation, List<SmartHomeDevice>> map = groupByLocation(deviceList, false);
        List<SmartHomeDevice> result = new ArrayList<>();
        for (List<SmartHomeDevice> smartHomeDevices : map.values()) {
            result.addAll(smartHomeDevices);
        }
        return result;
    }
}
