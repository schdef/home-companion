package de.stefanheintz.smarthome.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.wearable.DataMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class SmartHomeDevice implements Comparable<SmartHomeDevice>, Parcelable, Serializable {

    public static final int SWITCH = 0;
    public static final int TEMPERATURE = 1;
    public static final int TEMPERATURE_HEATING = 2;
    public static final int RELAY = 3;
    public static final String KEY_TYPE = "type";
    private static final String KEY_NAME = "name";
    private static final String KEY_LOCATION = "location";
    private static final String KEY_STATE = "state";
    private static final String KEY_ALIAS = "alias";
    private static final String KEY_FAV = "fav";
    private static final String KEY_VISIBLE = "visible";
    protected String name;
    protected List<SmartHomeLocation> locations = new ArrayList<>();
    protected String state;
    protected String alias;
    protected int fav = 0;
    protected int visible = 1;
    protected int type = SWITCH;

    public SmartHomeDevice() {
    }

    public SmartHomeDevice(String name, SmartHomeLocation location, String state) {
        this(name, new ArrayList<SmartHomeLocation>(), state);
        this.locations.add(location);
    }

    public SmartHomeDevice(String name, List<SmartHomeLocation> locations, String state) {
        this.name = name;
        if (locations == null) {
            this.locations = new ArrayList<>();
        } else {
            this.locations = locations;
        }
        this.state = state;
    }

    public SmartHomeDevice(DataMap map) {
        this(map.getString(KEY_NAME), new ArrayList<SmartHomeLocation>(), map.getString(KEY_STATE));
        ArrayList<DataMap> dataMapArrayList = map.getDataMapArrayList(KEY_LOCATION);
        locations = new ArrayList<>();
        if (dataMapArrayList != null) {
            for (DataMap dataMap : dataMapArrayList) {
                locations.add(SmartHomeLocation.fromDataMap(dataMap));
            }
        }
        setAlias(map.getString(KEY_ALIAS));
        this.fav = map.getInt(KEY_FAV);
        this.visible = map.getInt(KEY_VISIBLE);
        this.type = map.getInt(KEY_TYPE);
    }

    protected SmartHomeDevice(Parcel in) {
        name = in.readString();
        in.readTypedList(locations, SmartHomeLocation.CREATOR);
        state = in.readString();
        alias = in.readString();
        fav = in.readInt();
        visible = in.readInt();
        type = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeTypedList(locations);
        dest.writeString(state);
        dest.writeString(alias);
        dest.writeInt(fav);
        dest.writeInt(visible);
        dest.writeInt(type);
    }

    public static final Creator<SmartHomeDevice> CREATOR = new Creator<SmartHomeDevice>() {
        @Override
        public SmartHomeDevice createFromParcel(Parcel in) {
            return new SmartHomeDevice(in);
        }

        @Override
        public SmartHomeDevice[] newArray(int size) {
            return new SmartHomeDevice[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SmartHomeLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<SmartHomeLocation> locations) {
        this.locations = locations;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAlias() {
        return this.alias;
    }

    public SmartHomeDevice setAlias(String alias) {
        this.alias = alias;
        return this;
    }

    public boolean isVisible() {
        if (this.visible == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void setVisible(boolean visibleState) {
        if (visibleState) {
            visible = 1;
        } else {
            visible = 0;
        }
    }

    public boolean isFav() {
        if (this.fav == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void setFav(boolean favState) {
        if (favState) {
            fav = 1;
        } else {
            fav = 0;
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int compareTo(SmartHomeDevice another) {
        if (this.getAliasOrName() != null) {
            return -1 * another.getAliasOrName().compareTo(this.getAliasOrName());
        } else throw new IllegalArgumentException();
    }

    public DataMap toDataMap() {
        DataMap map = new DataMap();
        map.putString(KEY_NAME, this.name);
        ArrayList<DataMap> locationMapList = new ArrayList<DataMap>();
        for (SmartHomeLocation location : locations) {
            locationMapList.add(location.toDataMap());
        }
        map.putDataMapArrayList(KEY_LOCATION, locationMapList);
        map.putString(KEY_STATE, this.state);
        map.putString(KEY_ALIAS, this.alias);
        map.putInt(KEY_FAV, this.fav);
        map.putInt(KEY_VISIBLE, this.visible);
        map.putInt(KEY_TYPE, this.type);
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmartHomeDevice that = (SmartHomeDevice) o;

        if (fav != that.fav) return false;
        if (visible != that.visible) return false;
        if (type != that.type) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (locations != null ? !locations.equals(that.locations) : that.locations != null)
            return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        return !(alias != null ? !alias.equals(that.alias) : that.alias != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (locations != null ? locations.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (alias != null ? alias.hashCode() : 0);
        result = 31 * result + fav;
        result = 31 * result + visible;
        result = 31 * result + type;
        return result;
    }

    public String getAliasOrName() {
        return this.alias == null ? this.name : this.alias;
    }

    public String getLocationsAsString() {
        String result = "";
        if (locations != null) {
            boolean isFirst = true;
            for (SmartHomeLocation location : locations) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    result += ", ";
                }
                result += location != null ? location.getName() : "";
            }
        }
        return result;
    }

    public String getLocationForSwitchOrEmptyString() {
        if (this.locations != null && this.locations.size() > 0) {
            return this.locations.iterator().next().getName();
        }
        return "";
    }
}
