package de.stefanheintz.smarthome.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.wearable.DataMap;

import java.util.List;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class SwitchDevice extends SmartHomeDevice implements Comparable<SmartHomeDevice>, Parcelable {

    public static final Parcelable.Creator<SwitchDevice> CREATOR
            = new Parcelable.Creator<SwitchDevice>() {
        public SwitchDevice createFromParcel(Parcel in) {
            return new SwitchDevice(in);
        }

        public SwitchDevice[] newArray(int size) {
            return new SwitchDevice[size];
        }
    };

    public SwitchDevice() {
        super();
        setType(SWITCH);
    }

    public SwitchDevice(String name, SmartHomeLocation location, String state) {
        super(name, location, state);
        setType(SWITCH);
    }

    public SwitchDevice(String name, List<SmartHomeLocation> locations, String state) {
        super(name, locations, state);
        setType(SWITCH);
    }

    public SwitchDevice(DataMap map) {
        super(map);
        setType(SWITCH);
    }

    private SwitchDevice(Parcel in) {
        super(in);
        setType(SWITCH);
    }

}