package de.stefanheintz.smarthome.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.wearable.DataMap;

import java.util.List;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class TemperatureHeatingDevice extends SmartHomeDevice implements Comparable<SmartHomeDevice>, Parcelable {

    public static final Creator<TemperatureHeatingDevice> CREATOR
            = new Creator<TemperatureHeatingDevice>() {
        public TemperatureHeatingDevice createFromParcel(Parcel in) {
            return new TemperatureHeatingDevice(in);
        }

        public TemperatureHeatingDevice[] newArray(int size) {
            return new TemperatureHeatingDevice[size];
        }
    };
    private static final String KEY_DESIRED_TEMPERATURE = "desired_temperature";
    private static final String KEY_MEASURED_TEMPERATURE = "measured_temperature";
    private String desiredTemperature;
    private String measuredTemperature;

    public TemperatureHeatingDevice() {
        super();
        setType(TEMPERATURE_HEATING);
    }

    public TemperatureHeatingDevice(String name, SmartHomeLocation location, String state) {
        super(name, location, state);
        setType(TEMPERATURE_HEATING);
    }

    public TemperatureHeatingDevice(String name, List<SmartHomeLocation> locations, String state) {
        super(name, locations, state);
        setType(TEMPERATURE_HEATING);
    }

    public TemperatureHeatingDevice(DataMap map) {
        super(map);
        setType(TEMPERATURE_HEATING);
        this.desiredTemperature = map.getString(KEY_DESIRED_TEMPERATURE);
        this.measuredTemperature = map.getString(KEY_MEASURED_TEMPERATURE);
    }

    public DataMap toDataMap() {
        DataMap map = super.toDataMap();
        map.putString(KEY_DESIRED_TEMPERATURE, this.desiredTemperature);
        map.putString(KEY_MEASURED_TEMPERATURE, this.measuredTemperature);
        return map;
    }

    private TemperatureHeatingDevice(Parcel in) {
        super(in);
        desiredTemperature = in.readString();
        measuredTemperature = in.readString();
        setType(TEMPERATURE_HEATING);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(desiredTemperature);
        dest.writeString(measuredTemperature);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TemperatureHeatingDevice that = (TemperatureHeatingDevice) o;

        if (desiredTemperature != null ? !desiredTemperature.equals(that.desiredTemperature) : that.desiredTemperature != null)
            return false;
        return !(measuredTemperature != null ? !measuredTemperature.equals(that.measuredTemperature) : that.measuredTemperature != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (desiredTemperature != null ? desiredTemperature.hashCode() : 0);
        result = 31 * result + (measuredTemperature != null ? measuredTemperature.hashCode() : 0);
        return result;
    }

    public String getDesiredTemperature() {
        return desiredTemperature;
    }

    public void setDesiredTemperature(String desiredTemperature) {
        this.desiredTemperature = desiredTemperature;
    }

    public String getMeasuredTemperature() {
        return measuredTemperature;
    }

    public void setMeasuredTemperature(String measuredTemperature) {
        this.measuredTemperature = measuredTemperature;
    }
}
