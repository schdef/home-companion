package de.stefanheintz.smarthome.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.wearable.DataMap;

import java.util.List;

import de.stefanheintz.smarthome.common.TemperatureUtil;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class TemperatureDevice extends SmartHomeDevice implements Comparable<SmartHomeDevice>, Parcelable {

    public static final Creator<TemperatureDevice> CREATOR
            = new Creator<TemperatureDevice>() {
        public TemperatureDevice createFromParcel(Parcel in) {
            return new TemperatureDevice(in);
        }

        public TemperatureDevice[] newArray(int size) {
            return new TemperatureDevice[size];
        }
    };
    private static final String KEY_TEMPERATURE = "temperature";
    private static final String KEY_HUMIDITY = "humidity";
    private String temperature;
    private String humidity;

    public TemperatureDevice() {
        super();
        setType(TEMPERATURE);
    }

    public TemperatureDevice(String name, SmartHomeLocation location, String state) {
        super(name, location, state);
        setType(TEMPERATURE);
    }

    public TemperatureDevice(String name, List<SmartHomeLocation> locations, String state) {
        super(name, locations, state);
        setType(TEMPERATURE);
    }

    public TemperatureDevice(DataMap map) {
        super(map);
        setType(TEMPERATURE);
        this.temperature = map.getString(KEY_TEMPERATURE);
        this.humidity = map.getString(KEY_HUMIDITY);
    }

    private TemperatureDevice(Parcel in) {
        super(in);
        setType(TEMPERATURE);
        temperature = in.readString();
        humidity = in.readString();
    }

    public DataMap toDataMap() {
        DataMap map = super.toDataMap();
        map.putString(KEY_TEMPERATURE, this.temperature);
        map.putString(KEY_HUMIDITY, this.humidity);
        return map;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(temperature);
        dest.writeString(humidity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TemperatureDevice that = (TemperatureDevice) o;

        if (temperature != null ? !temperature.equals(that.temperature) : that.temperature != null)
            return false;
        if (humidity != null ? !humidity.equals(that.humidity) : that.humidity != null)
            return false;
        return o.equals(that);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (temperature != null ? temperature.hashCode() : 0);
        result = 31 * result + (humidity != null ? humidity.hashCode() : 0);
        return result;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getTemperatureFormatted() {
        return TemperatureUtil.formattedTemperatureValue(temperature);
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }
}
