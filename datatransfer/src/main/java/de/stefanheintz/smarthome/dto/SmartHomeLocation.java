package de.stefanheintz.smarthome.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.wearable.DataMap;

import java.io.Serializable;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class SmartHomeLocation implements Parcelable, Comparable, Serializable {

    public static final String ROOM_NAME_FAVORITES = "Favorites";
    public static final String ROOM_NAME_ALL = "All";

    public static final Parcelable.Creator<SmartHomeLocation> CREATOR
            = new Parcelable.Creator<SmartHomeLocation>() {
        public SmartHomeLocation createFromParcel(Parcel in) {
            return new SmartHomeLocation(in);
        }

        public SmartHomeLocation[] newArray(int size) {
            return new SmartHomeLocation[size];
        }
    };

    private static final String KEY_NAME = "name";

    private String location;

    public SmartHomeLocation(String location) {
        this.location = location;
    }

    private SmartHomeLocation(Parcel in) {
        location = in.readString();
    }

    public static SmartHomeLocation fromDataMap(DataMap map) {
        return new SmartHomeLocation(map.getString(KEY_NAME));
    }

    public String getName() {
        return this.location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmartHomeLocation that = (SmartHomeLocation) o;

        return !(location != null ? !location.equals(that.location) : that.location != null);

    }

    @Override
    public int hashCode() {
        return location != null ? location.hashCode() : 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(location);
    }

    @Override
    public int compareTo(Object another) {
        SmartHomeLocation another1 = (SmartHomeLocation) another;
        if (this == another) return 0;
        if (this.getName() == null && another1 == null) {
            return 0;
        }
        if (this.getName() != null && another1 == null) {
            return -1;
        }
        if (this.getName() == null && another1 != null) {
            return 1;
        }
        if (this.getName().equalsIgnoreCase("favorites")) {
            return -1;
        }
        if (another1.getName().equalsIgnoreCase("favorites")) {
            return 1;
        }


        return this.getName().toLowerCase().compareTo(another1.getName().toLowerCase());
    }

    public DataMap toDataMap() {
        DataMap dataMap = new DataMap();
        dataMap.putString(KEY_NAME, this.location);
        return dataMap;
    }
}
