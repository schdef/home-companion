package de.stefanheintz.smarthome.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.wearable.DataMap;

import java.util.List;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class RelayDevice extends SmartHomeDevice implements Comparable<SmartHomeDevice>, Parcelable {

    public static final Creator<RelayDevice> CREATOR
            = new Creator<RelayDevice>() {
        public RelayDevice createFromParcel(Parcel in) {
            return new RelayDevice(in);
        }

        public RelayDevice[] newArray(int size) {
            return new RelayDevice[size];
        }
    };
    private static final String KEY_PREVIOUS_TIME = "previous_time";
    private static final String KEY_PREVIOUS_STATE = "previous_state";
    private static final String KEY_STATE_TIME = "state_time";
    private String stateTime;
    private String previousTime;
    private String previousState;

    public RelayDevice() {
        super();
        setType(RELAY);
    }

    public RelayDevice(String name, SmartHomeLocation location, String state) {
        super(name, location, state);
        setType(RELAY);
    }

    public RelayDevice(String name, List<SmartHomeLocation> locations, String state) {
        super(name, locations, state);
        setType(RELAY);
    }

    public RelayDevice(DataMap map) {
        super(map);
        setType(RELAY);
        this.stateTime = map.getString(KEY_STATE_TIME);
        this.previousTime = map.getString(KEY_PREVIOUS_TIME);
        this.previousState = map.getString(KEY_PREVIOUS_STATE);
    }

    @Override
    public DataMap toDataMap() {
        DataMap map = super.toDataMap();
        map.putString(KEY_STATE_TIME, this.stateTime);
        map.putString(KEY_PREVIOUS_TIME, this.previousTime);
        map.putString(KEY_PREVIOUS_STATE, this.previousState);
        return map;
    }

    private RelayDevice(Parcel in) {
        super(in);
        setType(RELAY);
        this.stateTime = in.readString();
        this.previousTime = in.readString();
        this.previousState = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.stateTime);
        dest.writeString(this.previousTime);
        dest.writeString(this.previousState);
    }

    public void setPreviousTime(String previousTime) {
        this.previousTime = previousTime;
    }

    public String getPreviousTime() {
        return previousTime;
    }

    public String getPreviousState() {
        return previousState;
    }

    public void setPreviousState(String previousState) {
        this.previousState = previousState;
    }

    public String getStateTime() {
        return stateTime;
    }

    public void setStateTime(String stateTime) {
        this.stateTime = stateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RelayDevice that = (RelayDevice) o;

        if (stateTime != null ? !stateTime.equals(that.stateTime) : that.stateTime != null)
            return false;
        if (previousTime != null ? !previousTime.equals(that.previousTime) : that.previousTime != null)
            return false;
        return !(previousState != null ? !previousState.equals(that.previousState) : that.previousState != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (stateTime != null ? stateTime.hashCode() : 0);
        result = 31 * result + (previousTime != null ? previousTime.hashCode() : 0);
        result = 31 * result + (previousState != null ? previousState.hashCode() : 0);
        return result;
    }
}