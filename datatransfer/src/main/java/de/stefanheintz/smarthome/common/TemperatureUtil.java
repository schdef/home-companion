package de.stefanheintz.smarthome.common;

import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.logging.SimpleFormatter;

import de.stefanheintz.smarthome.dto.R;
import de.stefanheintz.smarthome.dto.TemperatureDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class TemperatureUtil {

    public static final String DEGREE_SIGN = "\u00B0";

    public static void configureTemperatureColor(Resources res, TextView textView, String temperatureAsString) {
        try {
            GradientDrawable background = (GradientDrawable) textView.getBackground();
            float temperature = Float.parseFloat(temperatureAsString);
            textView.setTextColor(res.getColor(R.color.white));
            if (temperature >= 30) {
                background.setColor(res.getColor(R.color.temp_red));
            } else if (temperature >= 20) {
                background.setColor(res.getColor(R.color.temp_egg));
                textView.setTextColor(res.getColor(R.color.black));
            } else if (temperature >= 10) {
                background.setColor(res.getColor(R.color.temp_green));
                textView.setTextColor(res.getColor(R.color.black));
            } else if (temperature >= 0) {
                textView.setTextColor(res.getColor(R.color.black));
                background.setColor(res.getColor(R.color.temp_ice));
            } else if (temperature >= -10 ) {
                background.setColor(res.getColor(R.color.temp_blue));
            } else if (temperature >= -20 ) {
                background.setColor(res.getColor(R.color.temp_dark_blue));
            } else {
                background.setColor(res.getColor(R.color.temp_violet));
            }
        } catch (Throwable t) {

        }
    }

    public static String formattedTemperatureValue(String unformattedTemperature) {
        if(unformattedTemperature != null) {
            try {
                NumberFormat nf = NumberFormat.getInstance();
                nf.setMaximumFractionDigits(1);
                String formatted = nf.format(Double.parseDouble(unformattedTemperature.replace(" ", "")));
                return formatted.replace(",", ".");
            } catch (Exception e) {
                return "?";
            }
        }
        return unformattedTemperature;
    }
}
