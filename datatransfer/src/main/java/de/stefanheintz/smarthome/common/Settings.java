package de.stefanheintz.smarthome.common;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class Settings {

    public static final String PREF_FAV_LOCATION = "favLocation";

    public static final String PREF_KEY_NO_FULL_OR_LIST_VIEW = "pref_key_no_full_or_list_view";
    public static final String PREF_KEY_EN_DIS_DEBUG_MODE = "pref_key_en_dis_debug_mode";
    public static final String PREF_DEFAULT_NO_FULL_OR_LIST_VIEW = "4";
    public static final String PREF_KEY_ROOM_FILTER = "pref_key_room_filter";
    public static final String PREF_KEY_SYSTEM_PREFERENCE = "system_preference";
    public static final String PREF_KEY_SYSTEM_CONFIG = "pref_key_system_config";
}
