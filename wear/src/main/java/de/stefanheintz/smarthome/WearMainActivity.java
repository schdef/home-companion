package de.stefanheintz.smarthome;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.wearable.view.DotsPageIndicator;
import android.support.wearable.view.GridViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowInsets;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.dto.GroupingUtil;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.dto.Utils;
import de.stefanheintz.smarthome.uihelper.LastRoomPosition;
import de.stefanheintz.smarthome.uihelper.RoomGridPagerAdapter;
import de.stefanheintz.smarthome.util.BackendCommunication;
import de.stefanheintz.smarthome.util.AppUtil;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class WearMainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnTouchListener {

    public static final String PREF_HOCO_SETTINGS = "HoCoSettings";

    boolean showLastRoom = true;
    Boolean pref_key_en_dis_debug_mode = false;

    private GoogleApiClient mGoogleApiClient;
    private BackendCommunication backend;
    private WearApplication application;
    public SharedPreferences settings;

    private GridViewPager pager;
    private DotsPageIndicator dotsPageIndicator;
    private Rect mInsets = new Rect(0, 0, 0, 0);

    private TextView syncLabel;
    private LinearLayout appInfoLayout;
    private LinearLayout waitingScreenLayout;
    private GestureDetector detector;

    private Handler handler = new Handler();
    private MessageReceiver messageReceiver = new MessageReceiver();
    private String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pager = (GridViewPager) findViewById(R.id.pager);
        pager.setOnTouchListener(this);
        dotsPageIndicator = (DotsPageIndicator) findViewById(R.id.page_indicator);
        View.OnClickListener refreshClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh(null);
            }
        };
        appInfoLayout = (LinearLayout) findViewById(R.id.app_info_layout);
        appInfoLayout.setOnClickListener(refreshClickListener);
        syncLabel = (TextView) findViewById(R.id.label);
        syncLabel.setOnClickListener(refreshClickListener);
        syncLabel.setText(getResources().getString(R.string.app_name) + AppUtil.getCurrentAppVersion(getApplicationContext()));
        waitingScreenLayout = (LinearLayout) findViewById(R.id.waiting_screen_layout);
        waitingScreenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                waitingScreenLayout.setVisibility(View.GONE);
            }
        });
        detector = new GestureDetector(getApplicationContext(), new GestureListener());
        settings = getSharedPreferences(PREF_HOCO_SETTINGS, MODE_PRIVATE);

        final RoomGridPagerAdapter adapter = new RoomGridPagerAdapter(this, getFragmentManager(), this);
        pager.setAdapter(adapter);

        dotsPageIndicator.setPager(pager);

        pager.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
            @Override
            public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
                // Call through to super implementation
                insets = pager.onApplyWindowInsets(insets);
                boolean round = insets.isRound();

                // Store system window insets regardless of screen shape
                mInsets.set(insets.getSystemWindowInsetLeft(),
                        insets.getSystemWindowInsetTop(),
                        insets.getSystemWindowInsetRight(),
                        insets.getSystemWindowInsetBottom());

                if (round) {
                    // On a round screen calculate the square inset to use.
                    // Alternatively could use BoxInsetLayout, although calculating
                    // the inset ourselves lets us position views outside the center
                    // box. For example, slightly lower on the round screen (by giving
                    // up some horizontal space).
                    mInsets = Utils.calculateBottomInsetsOnRoundDevice(
                            getWindowManager().getDefaultDisplay(), mInsets);

                    // Boost the dots indicator up by the bottom inset
                    FrameLayout.LayoutParams params =
                            (FrameLayout.LayoutParams) dotsPageIndicator.getLayoutParams();
                    params.bottomMargin = mInsets.bottom;
                    dotsPageIndicator.setLayoutParams(params);
                }

                return insets;
            }
        });


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        backend = new BackendCommunication(mGoogleApiClient, getApplicationContext());
        application = (WearApplication) getApplication();
        application.setBackendCommunication(backend);
    }

    @Override
    protected void onResume() {
        Log.v(TAG, "onResume");
        super.onResume();

        IntentFilter messageFilter = new IntentFilter(Intent.ACTION_SEND);
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, messageFilter);

        mGoogleApiClient.connect();
        settings = getSharedPreferences(PREF_HOCO_SETTINGS, MODE_PRIVATE);
        refreshView(null);
        backend.refreshAll();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Log.v(TAG, "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    protected void onStop() {
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        LastRoomPosition.saveLastRoomPosition(pager, getRoomList(), settings);

        super.onStop();
    }

    public void logDebug(String message) {
        if(pref_key_en_dis_debug_mode) {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    private void refreshView(final SmartHomeDevice device) {
        final Map<SmartHomeLocation, List<SmartHomeDevice>> roomList = getRoomList();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                endWaitingScreen();
                if (device == null) {
                    if (showLastRoom) {
                        pager.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                            @Override
                            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                                LastRoomPosition.setViewToLastRoomPosition(pager, roomList, settings);
                                showLastRoom = false;
                                pager.getAdapter().notifyDataSetChanged();
                                pager.removeOnLayoutChangeListener(this);
                            }
                        });
                    }
                    ((RoomGridPagerAdapter) pager.getAdapter()).refill(((WearApplication) getApplication()).getCachedDeviceList(settings), roomList);
                    if (roomList.size() == 0) {
                        appInfoLayout.setVisibility(View.VISIBLE);
                    } else {
                        appInfoLayout.setVisibility(View.GONE);
                    }
                } else {
                    ((RoomGridPagerAdapter) pager.getAdapter()).refill(device);
                }
            }
        });

    }

    public void refresh(SmartHomeDevice theDevice) {
        showWaitingScreen();
        if(theDevice != null) {
            backend.refreshDevice(theDevice);
        } else {
            backend.refreshAll();
        }
    }

    private void showWaitingScreen() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                endWaitingScreen();
            }
        }, 3000);
        appInfoLayout.setVisibility(View.GONE);
        waitingScreenLayout.setVisibility(View.VISIBLE);
    }

    private void endWaitingScreen() {
        waitingScreenLayout.setVisibility(View.INVISIBLE);
    }

    public void triggerDeviceAction(SmartHomeDevice device) {
        showWaitingScreen();
        String command = backend.triggerDeviceAction(device);
        logDebug("Triggered command: " + command);
    }

    public Map<SmartHomeLocation, List<SmartHomeDevice>> getRoomList() {
        List<SmartHomeDevice> deviceList = ((WearApplication) getApplication()).getCachedDeviceList(settings);
        if (deviceList == null || deviceList.size() == 0) {
            TreeMap<SmartHomeLocation, List<SmartHomeDevice>> roomList = new TreeMap<>();
            return roomList;
        }
        Map<SmartHomeLocation, List<SmartHomeDevice>> map = GroupingUtil.groupByLocation(deviceList);
        map = applyRoomFilter(map);

        return map;
    }

    private Map<SmartHomeLocation, List<SmartHomeDevice>> applyRoomFilter(Map<SmartHomeLocation, List<SmartHomeDevice>> map) {
        String roomFilterValue = settings.getString(Settings.PREF_KEY_ROOM_FILTER, null);
        Map<SmartHomeLocation, List<SmartHomeDevice>> result = new TreeMap<>();
        if (roomFilterValue != null) {
            for (Map.Entry<SmartHomeLocation, List<SmartHomeDevice>> entry : map.entrySet()) {
                if(entry.getKey().getName().equalsIgnoreCase(roomFilterValue) || entry.getKey().getName().equalsIgnoreCase(SmartHomeLocation.ROOM_NAME_FAVORITES)) {
                    result.put(entry.getKey(), entry.getValue());
                }
            }
            return result;
        } else {
            return map;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return detector.onTouchEvent(motionEvent);
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if(appInfoLayout.getVisibility() == View.VISIBLE) {
                appInfoLayout.setVisibility(View.GONE);
            } else {
                appInfoLayout.setVisibility(View.VISIBLE);
            }
            return true;
        }
    }

    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v(TAG, "local message receiver");
            SmartHomeDevice device = (SmartHomeDevice) intent.getParcelableExtra("device");
            refreshView(device);
        }
    }
}
