package de.stefanheintz.smarthome.uihelper;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.stefanheintz.smarthome.R;
import de.stefanheintz.smarthome.common.TemperatureUtil;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class TemperatureHeatingFragment extends Fragment implements View.OnClickListener, BaseFragment {

    private static final float DEFAULT_VALUE = 13.5f;
    private static Listener mListener;
    public TemperatureHeatingDevice device;
    private TextView currentTempTextView;
    private TextView desiredTempTextView;
    private TextView plusTextView;
    private TextView minusTextView;
    private TextView confirmTextView;
    private TextView vLabel;

    public static TemperatureHeatingFragment create(TemperatureHeatingDevice device, Listener listener) {
        mListener = listener;
        TemperatureHeatingFragment fragment = new TemperatureHeatingFragment();
        Bundle args = new Bundle();
        args.putParcelable("DEVICE", device);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.temperature_heating_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vLabel = (TextView) view.findViewById(R.id.label);
        currentTempTextView = (TextView) view.findViewById(R.id.currentTemp);
        desiredTempTextView = (TextView) view.findViewById(R.id.desiredTemp);
        plusTextView = (TextView) view.findViewById(R.id.plus);
        minusTextView = (TextView) view.findViewById(R.id.minus);
        confirmTextView = (TextView) view.findViewById(R.id.confirm);

        TemperatureHeatingDevice deviceParcel = getArguments().getParcelable("DEVICE");
        if(deviceParcel != null) {
            this.device = deviceParcel;
        }
        update(this.device);

        plusTextView.setOnClickListener(this);
        minusTextView.setOnClickListener(this);
        confirmTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(plusTextView)){
            float desiredTemp = readDesiredTemp();
            desiredTemp += 0.5;
            desiredTempTextView.setText(String.valueOf(desiredTemp)+ TemperatureUtil.DEGREE_SIGN);

        } else if(v.equals(minusTextView)){
            float desiredTemp = readDesiredTemp();
            desiredTemp -= 0.5;
            desiredTempTextView.setText(String.valueOf(desiredTemp)+ TemperatureUtil.DEGREE_SIGN);

        } else if(v.equals(confirmTextView)){
            if (mListener != null) {
                device.setDesiredTemperature(String.valueOf(readDesiredTemp()));
                mListener.onActionPerformed(device);
            }
        }
    }

    private float readDesiredTemp() {

        String s = desiredTempTextView.getText().toString();
        String replaced = s.replace(TemperatureUtil.DEGREE_SIGN, "").replace("off", ""+DEFAULT_VALUE);
        try {
            return Float.parseFloat(replaced);
        } catch(Exception e) {
            return 13.5f;
        }
    }

    @Override
    public void update(SmartHomeDevice newDevice) {
        device = (TemperatureHeatingDevice) newDevice;
        vLabel.setText(device.getAliasOrName());
        currentTempTextView.setText(TemperatureUtil.formattedTemperatureValue(device.getMeasuredTemperature())+TemperatureUtil.DEGREE_SIGN);
        desiredTempTextView.setText(TemperatureUtil.formattedTemperatureValue(device.getDesiredTemperature()) + TemperatureUtil.DEGREE_SIGN);
    }

    @Override
    public SmartHomeDevice getDevice() {
        return device;
    }

    public interface Listener {
        public void onActionPerformed(TemperatureHeatingDevice theDevice);
    }
}