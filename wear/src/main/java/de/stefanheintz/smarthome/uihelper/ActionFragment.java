package de.stefanheintz.smarthome.uihelper;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.wearable.view.CircledImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.stefanheintz.smarthome.R;
import de.stefanheintz.smarthome.WearMainActivity;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class ActionFragment extends Fragment implements View.OnClickListener, BaseFragment {

    private static Listener mListener;
    private CircledImageView vIcon;
    private TextView vLabel;
    public SmartHomeDevice device;
    private WearMainActivity activity;

    public static ActionFragment create(SmartHomeDevice device, Listener listener) {
        mListener = listener;
        ActionFragment fragment = new ActionFragment();
        Bundle args = new Bundle();
        args.putParcelable("DEVICE", device);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (WearMainActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_action, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        device = getArguments().getParcelable("DEVICE");
        vIcon = (CircledImageView) view.findViewById(R.id.icon);
        vLabel = (TextView) view.findViewById(R.id.label);
        vIcon.setImageResource(R.drawable.power_big);

        update(device);
        view.setOnClickListener(this);
        vIcon.setOnClickListener(this);
        vLabel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(mListener != null) {
            mListener.onActionPerformed(device);
        }
    }

    @Override
    public void update(SmartHomeDevice newDevice) {
        //Toast.makeText(this.activity, "update " + newDevice.getAliasOrName(), Toast.LENGTH_SHORT).show();
        device = newDevice;
        if (device.getState().equalsIgnoreCase("on")) {
            vIcon.setCircleColorStateList(activity.getResources().getColorStateList(R.color.action_button_green));
        } else {
            vIcon.setCircleColorStateList(activity.getResources().getColorStateList(R.color.action_button_red));
        }

        vLabel.setText(device.getAliasOrName());
    }

    @Override
    public SmartHomeDevice getDevice() {
        return device;
    }

    public interface Listener {
        public void onActionPerformed(SmartHomeDevice device);
    }
}