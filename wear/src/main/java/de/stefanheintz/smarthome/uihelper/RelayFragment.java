package de.stefanheintz.smarthome.uihelper;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.wearable.view.CircledImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.stefanheintz.smarthome.R;
import de.stefanheintz.smarthome.common.TemperatureUtil;
import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class RelayFragment extends Fragment implements View.OnClickListener, BaseFragment {

    private static Listener mListener;
    public RelayDevice device;
    private TextView icon;
    private TextView label;
    private Activity activity;

    public static RelayFragment create(RelayDevice device, Listener listener) {
        mListener = listener;
        RelayFragment fragment = new RelayFragment();
        Bundle args = new Bundle();
        args.putParcelable("DEVICE", device);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.relay_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        icon = (TextView) view.findViewById(R.id.icon);
        label = (TextView) view.findViewById(R.id.label);

        device = getArguments().getParcelable("DEVICE");
        update(device);

        view.setOnClickListener(this);
        icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(mListener != null) {
            mListener.onActionPerformed();
        }
    }

    @Override
    public void update(SmartHomeDevice newDevice) {
        device = (RelayDevice) newDevice;
        label.setText(device.getAliasOrName());
        if("open".equalsIgnoreCase(device.getState())) {
            icon.setBackgroundResource(R.drawable.open);
        } else {
            icon.setBackgroundResource(R.drawable.close);
        }
        icon.setText(device.getState());
    }

    @Override
    public SmartHomeDevice getDevice() {
        return device;
    }

    public interface Listener {
        public void onActionPerformed();
    }
}