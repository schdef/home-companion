package de.stefanheintz.smarthome.uihelper;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.support.wearable.view.GridPagerAdapter;
import android.view.Gravity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.stefanheintz.smarthome.R;
import de.stefanheintz.smarthome.WearMainActivity;
import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.common.TemperatureUtil;
import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class RoomGridPagerAdapter extends FragmentGridPagerAdapter {

    static final int[] BG_IMAGES = new int[]{
            R.drawable.background
    };
    private static final int TRANSITION_DURATION_MILLIS = 100;
    int SWITTCH_VALUE;

    private final Context mContext;
    private final WearMainActivity activity;
    private List<Row> roomRows;
    private ColorDrawable mDefaultBg;
    LruCache<Integer, Drawable> mRowBackgrounds = new LruCache<Integer, Drawable>(3) {
        @Override
        protected Drawable create(final Integer row) {
            int resid = BG_IMAGES[row % BG_IMAGES.length];
            new DrawableLoadingTask(mContext) {
                @Override
                protected void onPostExecute(Drawable result) {
                    TransitionDrawable background = new TransitionDrawable(new Drawable[]{
                            mDefaultBg,
                            result
                    });
                    mRowBackgrounds.put(row, background);
                    notifyRowBackgroundChanged(row);
                    background.startTransition(TRANSITION_DURATION_MILLIS);
                }
            }.execute(resid);
            return mDefaultBg;
        }
    };
    private ColorDrawable mClearBg;
    LruCache<Point, Drawable> mPageBackgrounds = new LruCache<Point, Drawable>(3) {
        @Override
        protected Drawable create(final Point page) {
            if (page.x >= 1) {
                int resid = R.drawable.black;
                new DrawableLoadingTask(mContext) {
                    @Override
                    protected void onPostExecute(Drawable result) {
                        TransitionDrawable background = new TransitionDrawable(new Drawable[]{
                                mClearBg,
                                new ColorDrawable(activity.getResources().getColor(R.color.black))
                        });
                        mPageBackgrounds.put(page, background);
                        notifyPageBackgroundChanged(page.y, page.x);
                        background.startTransition(TRANSITION_DURATION_MILLIS);
                    }
                }.execute(resid);
            }
            return GridPagerAdapter.BACKGROUND_NONE;
        }
    };

    public RoomGridPagerAdapter(Context ctx, FragmentManager fm, WearMainActivity activity) {
        super(fm);
        this.activity = activity;
        mContext = ctx;

        roomRows = new ArrayList<Row>();
        Map<SmartHomeLocation, List<SmartHomeDevice>> roomList = activity.getRoomList();

        for (SmartHomeLocation smartHomeLocation : roomList.keySet()) {
            List<SmartHomeDevice> devices = roomList.get(smartHomeLocation);
            addFragment(smartHomeLocation, devices);
        }

        mDefaultBg = new ColorDrawable(activity.getResources().getColor(R.color.app_blue));
        mClearBg = new ColorDrawable(activity.getResources().getColor(android.R.color.transparent));

        try {
            String switchValueString = this.activity.settings.getString(Settings.PREF_KEY_NO_FULL_OR_LIST_VIEW, Settings.PREF_DEFAULT_NO_FULL_OR_LIST_VIEW);
            SWITTCH_VALUE = Integer.parseInt(switchValueString);
        } catch(Exception e) {
        }
    }

    private void addFragment(SmartHomeLocation smartHomeLocation, List<SmartHomeDevice> devices) {

        CustomFragment frag = new CustomFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("location", smartHomeLocation);
        frag.setArguments(bundle);
        String title = smartHomeLocation.getName();

        Row row = new Row(cardFragment(title, deviceText(devices)));
        if(devices.size() >= SWITTCH_VALUE) {
            row.add(frag);
        } else {
            for (final SmartHomeDevice device : devices) {
                if (device instanceof TemperatureDevice) {
                    TemperatureFragment fragment = TemperatureFragment.create(((TemperatureDevice) device), new TemperatureFragment.Listener() {
                        @Override
                        public void onActionPerformed() {
                            activity.refresh(device);
                        }
                    });
                    row.add(fragment);
                } else if (device instanceof TemperatureHeatingDevice) {
                    TemperatureHeatingFragment fragment = TemperatureHeatingFragment.create(((TemperatureHeatingDevice) device), new TemperatureHeatingFragment.Listener() {
                        @Override
                        public void onActionPerformed(final TemperatureHeatingDevice theDevice) {
                            activity.triggerDeviceAction(theDevice);
                        }
                    });
                    row.add(fragment);
                } else if (device instanceof RelayDevice) {
                    RelayFragment fragment = RelayFragment.create(((RelayDevice) device), new RelayFragment.Listener() {
                        @Override
                        public void onActionPerformed() {
                            activity.refresh(device);
                        }
                    });
                    row.add(fragment);
                } else if (device instanceof SwitchDevice) {
                    ActionFragment actionFragment = ActionFragment.create(device, new ActionFragment.Listener() {
                        @Override
                        public void onActionPerformed(final SmartHomeDevice theDevice) {
                            activity.triggerDeviceAction(theDevice);
                        }
                    });
                    row.add(actionFragment);
                }
            }
        }
        roomRows.add(row);
    }

    private String deviceText(List<SmartHomeDevice> devices) {
        String result = "";
        if (devices == null) {
            return result;
        }
        for (SmartHomeDevice device : devices) {
            result += device.getAliasOrName();
            result += "\n";
        }
        return result;
    }

    private Fragment cardFragment(String title, String description) {
        Resources res = mContext.getResources();
        CustomCardFragment fragment = new CustomCardFragment();
        Bundle args = new Bundle();
        if (title != null) {
            args.putCharSequence("CardFragment_title", title);
        }

        if (description != null) {
            args.putCharSequence("CardFragment_text", description);
        }

        fragment.setArguments(args);
        // Add some extra bottom margin to leave room for the page indicator
        fragment.setCardMarginBottom(res.getDimensionPixelSize(R.dimen.card_margin_bottom));
        fragment.setCardMarginTop(res.getDimensionPixelSize(R.dimen.card_margin_bottom));
        fragment.setCardGravity(Gravity.BOTTOM);
        fragment.setExpansionEnabled(false);

        return fragment;
    }

    @Override
    public Fragment getFragment(int row, int col) {
        Row adapterRow = roomRows.get(row);
        return adapterRow.getColumn(col);
    }

    @Override
    public Drawable getBackgroundForRow(final int row) {
        return mRowBackgrounds.get(row);
    }

    @Override
    public Drawable getBackgroundForPage(final int row, final int column) {
        return mPageBackgrounds.get(new Point(column, row));
    }

    @Override
    public int getRowCount() {
        return roomRows.size();
    }

    @Override
    public int getColumnCount(int rowNum) {
        return roomRows.get(rowNum).getColumnCount();
    }

    public void refill(SmartHomeDevice smartHomeDevice) {
        for (Row mRow : roomRows) {
            for (Fragment frag : mRow.columns) {
                if (frag instanceof CustomFragment) {
                    CustomFragment cFrag = (CustomFragment) frag;
                    for (SmartHomeDevice homeDevice : cFrag.deviceList) {
                        if (homeDevice.getName().equalsIgnoreCase(smartHomeDevice.getName())
                                && homeDevice.getLocationsAsString().equalsIgnoreCase(smartHomeDevice.getLocationsAsString())) {
                            cFrag.update(smartHomeDevice);
                        }
                    }
                } else if ((frag instanceof TemperatureFragment) || (frag instanceof ActionFragment) || (frag instanceof TemperatureHeatingFragment)) {
                    BaseFragment tFrag = ((BaseFragment) frag);
                    SmartHomeDevice homeDevice = tFrag.getDevice();
                    if (homeDevice != null) {
                        if (homeDevice.getName().equals(smartHomeDevice.getName())
                                && homeDevice.getLocationsAsString().equals(smartHomeDevice.getLocationsAsString())) {
                            tFrag.update(smartHomeDevice);
                        }
                    }
                }
            }
        }
    }

    public void refill(List<SmartHomeDevice> deviceList, Map<SmartHomeLocation, List<SmartHomeDevice>> roomList) {
        boolean updateDataSet = false;

        if (roomList.size() != roomRows.size()) {
            updateDataSet = true;
            roomRows = new ArrayList<Row>();

           for (SmartHomeLocation smartHomeLocation : roomList.keySet()) {
                List<SmartHomeDevice> devices = roomList.get(smartHomeLocation);
                addFragment(smartHomeLocation, devices);
            }
        }

        for (Row mRow : roomRows) {
            for (Fragment frag : mRow.columns) {
                if (frag instanceof CustomFragment) {
                    CustomFragment cFrag = (CustomFragment) frag;
                    if (cFrag.location != null) {
                        //Log.d("RoomGridPager", "location is: " + cFrag.location.getName());
                        for (Map.Entry<SmartHomeLocation, List<SmartHomeDevice>> smartHomeLocationListEntry : roomList.entrySet()) {
                            if (cFrag.location.getName().equalsIgnoreCase(smartHomeLocationListEntry.getKey().getName())) {
                                if (smartHomeLocationListEntry.getValue().size() != cFrag.deviceList.size()) {
                                    //Log.d("RoomGridPager", "different size of devices: " + smartHomeLocationListEntry.getValue().size() + " vs. " + cFrag.deviceList.size());
                                    cFrag.deviceList = smartHomeLocationListEntry.getValue();
                                }
                            }
                        }
                    }
                    List<SmartHomeDevice> homeDevices = cFrag.deviceList;

                    // TODO: Is this really required?
                    for (SmartHomeDevice homeDevice : homeDevices) {
                        for (SmartHomeDevice smartHomeDevice : deviceList) {
                            if (homeDevice.getName().equals(smartHomeDevice.getName())
                                    && homeDevice.getLocationsAsString().equals(smartHomeDevice.getLocationsAsString())) {
                                homeDevice.setState(smartHomeDevice.getState());
                                if(homeDevice instanceof TemperatureDevice) {
                                    ((TemperatureDevice)homeDevice).setHumidity(((TemperatureDevice)smartHomeDevice).getHumidity());
                                    ((TemperatureDevice)homeDevice).setTemperature(((TemperatureDevice)smartHomeDevice).getTemperatureFormatted());
                                } else if(homeDevice instanceof TemperatureHeatingDevice) {
                                    ((TemperatureHeatingDevice)homeDevice).setDesiredTemperature(TemperatureUtil.formattedTemperatureValue(((TemperatureHeatingDevice)smartHomeDevice).getDesiredTemperature()));
                                    ((TemperatureHeatingDevice)homeDevice).setMeasuredTemperature(TemperatureUtil.formattedTemperatureValue(((TemperatureHeatingDevice)smartHomeDevice).getMeasuredTemperature()));
                                }

                            }
                        }
                    }
                    cFrag.update(homeDevices);
                } else if ((frag instanceof TemperatureFragment) || (frag instanceof ActionFragment) || (frag instanceof TemperatureHeatingFragment)) {
                    BaseFragment tFrag = ((BaseFragment) frag);
                    SmartHomeDevice homeDevice = tFrag.getDevice();
                    if(homeDevice != null) {
                        for (SmartHomeDevice smartHomeDevice : deviceList) {
                            if (homeDevice.getName().equals(smartHomeDevice.getName())
                                    && homeDevice.getLocationsAsString().equals(smartHomeDevice.getLocationsAsString())) {
                                tFrag.update(smartHomeDevice);
                            }
                        }
                    }
                }
            }
        }
        if (updateDataSet) {
            notifyDataSetChanged();
        }
    }

    /**
     * A convenient container for a row of fragments.
     */
    private class Row {
        final List<Fragment> columns = new ArrayList<Fragment>();

        public Row(Fragment... fragments) {
            for (Fragment f : fragments) {
                add(f);
            }
        }

        public void add(Fragment f) {
            columns.add(f);
        }

        Fragment getColumn(int i) {
            return columns.get(i);
        }

        public int getColumnCount() {
            return columns.size();
        }
    }

    class DrawableLoadingTask extends AsyncTask<Integer, Void, Drawable> {
        private static final String TAG = "Loader";
        private Context context;

        DrawableLoadingTask(Context context) {
            this.context = context;
        }

        @Override
        protected Drawable doInBackground(Integer... params) {
//            Log.d(TAG, "Loading asset 0x" + Integer.toHexString(params[0]));
            return context.getResources().getDrawable(params[0]);
        }
    }
}
