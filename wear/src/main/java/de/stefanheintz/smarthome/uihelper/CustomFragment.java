/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.stefanheintz.smarthome.uihelper;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.wearable.view.WearableListView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.stefanheintz.smarthome.FragActivity;
import de.stefanheintz.smarthome.R;
import de.stefanheintz.smarthome.WearMainActivity;
import de.stefanheintz.smarthome.common.TemperatureUtil;
import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;
/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class CustomFragment extends Fragment implements WearableListView.ClickListener {

    List<SmartHomeDevice> deviceList = new ArrayList<SmartHomeDevice>();
    SmartHomeLocation location = null;
    private WearableListView mWearableListView;
    private WearMainActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_fragment, container, false);
        TextView room = (TextView) view.findViewById(R.id.roomTextView);
        if (getArguments() != null) {
            location = getArguments().getParcelable("location");
        }
        if (location != null) {
            room.setText(location.getName());
            Map<SmartHomeLocation, List<SmartHomeDevice>> roomList = this.activity.getRoomList();
            for (Map.Entry<SmartHomeLocation, List<SmartHomeDevice>> smartHomeLocationListEntry : roomList.entrySet()) {
                if (smartHomeLocationListEntry.getKey().equals(location)) {
                    deviceList = smartHomeLocationListEntry.getValue();
                }
            }

            if (deviceList == null) {
                deviceList = new ArrayList<SmartHomeDevice>();
            }
        }

        mWearableListView = (WearableListView) view.findViewById(R.id.wearable_list);
        mWearableListView.setAdapter(new Adapter(getActivity(), deviceList.toArray(new SmartHomeDevice[]{})));


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (WearMainActivity) activity;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mWearableListView.setGreedyTouchMode(true);
        mWearableListView.setClickListener(this);
    }

    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {
        Adapter.ItemViewHolder itemHolder = (Adapter.ItemViewHolder) viewHolder;
        Integer tag = (Integer) viewHolder.itemView.getTag();
        SmartHomeDevice device = deviceList.get(tag);
        if (deviceList != null && deviceList.size() > 0) {
            if(device instanceof SwitchDevice) {
                this.activity.triggerDeviceAction(deviceList.get(tag));
            } else {
                Intent intent = new Intent(getActivity(), FragActivity.class);
                intent.putExtra("DEVICE", ((Parcelable)device));
                startActivity(intent);
            }
        }
    }

    @Override
    public void onTopEmptyRegionClick() {
        //System.out.println("clicked onTopEmptyRegionClick");
        //((WearMainActivity2) getActivity()).refresh();
    }

    public void update(List<SmartHomeDevice> allDeviceList) {
        if (mWearableListView != null && mWearableListView.getAdapter() != null) {
            ((Adapter) mWearableListView.getAdapter()).refill(allDeviceList);
        } else {
        }
    }
    public void update(SmartHomeDevice newDevice) {
        if (mWearableListView != null && mWearableListView.getAdapter() != null) {
            ((Adapter) mWearableListView.getAdapter()).refill(newDevice);
        }
    }

    private static final class Adapter extends WearableListView.Adapter {
        private final Context mContext;
        private final LayoutInflater mInflater;
        private SmartHomeDevice[] mDataset;

        public Adapter(Context context, SmartHomeDevice[] dataset) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
            mDataset = dataset;
        }

        public void refill(List<SmartHomeDevice> deviceList) {
            mDataset = deviceList.toArray(new SmartHomeDevice[]{});
            notifyDataSetChanged();
        }

        public void refill(SmartHomeDevice newDevice) {
            int index = 0;
            for (SmartHomeDevice device : mDataset.clone()) {
                if(device.getAliasOrName().equalsIgnoreCase(newDevice.getAliasOrName()) &&
                        device.getLocationForSwitchOrEmptyString().equalsIgnoreCase(newDevice.getLocationForSwitchOrEmptyString())) {
                    mDataset[index] = newDevice;
                }
                index++;
            }
            notifyDataSetChanged();
        }

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            return new ItemViewHolder(mInflater.inflate(R.layout.list_item, null));
        }


        @Override
        public void onBindViewHolder(WearableListView.ViewHolder holder,
                                     int position) {
            SmartHomeDevice device = mDataset[position];

            //Log.v(TAG, "device-info: " + device.getName() + ", " + device.getState());

            ItemViewHolder itemHolder = (ItemViewHolder) holder;
            TextView view = itemHolder.textView;
            view.setText(device.getAliasOrName());
            itemHolder.imageView.setTextSize(15);

            if (device instanceof TemperatureDevice) {
                itemHolder.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.wl_circle));
                TemperatureDevice tDevice = ((TemperatureDevice) device);
                itemHolder.imageView.setText(tDevice.getTemperatureFormatted());
                TemperatureUtil.configureTemperatureColor(mContext.getResources(), itemHolder.imageView, tDevice.getTemperatureFormatted());
                view.setText(device.getAliasOrName());
            } else if (device instanceof TemperatureHeatingDevice) {
                itemHolder.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.wl_circle));
                TemperatureHeatingDevice tDevice = ((TemperatureHeatingDevice) device);
                itemHolder.imageView.setText(tDevice.getMeasuredTemperature());
                TemperatureUtil.configureTemperatureColor(mContext.getResources(), itemHolder.imageView, tDevice.getMeasuredTemperature());
                view.setText(device.getAliasOrName());
            } else if(device instanceof RelayDevice){
                itemHolder.imageView.setText(device.getState());
                itemHolder.imageView.setTextSize(13);
                itemHolder.imageView.setTextColor(mContext.getResources().getColor(R.color.white));
                if("open".equalsIgnoreCase(device.getState())) {
                    itemHolder.imageView.setBackgroundResource(R.drawable.open);
                } else {
                    itemHolder.imageView.setBackgroundResource(R.drawable.close);
                }
            } else {
                itemHolder.imageView.setText("");
                if (device.getState().equalsIgnoreCase("on")) {
                    itemHolder.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.on));
                } else {
                    itemHolder.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.off));
                }
            }

            holder.itemView.setTag(position);
        }

        @Override
        public int getItemCount() {
            return mDataset.length;
        }

        public static class ItemViewHolder extends WearableListView.ViewHolder {
            private TextView textView;
            private TextView imageView;

            public ItemViewHolder(View itemView) {
                super(itemView);
                textView = (TextView) itemView.findViewById(R.id.name);
                imageView = (TextView) itemView.findViewById(R.id.state);
            }
        }
    }
}
