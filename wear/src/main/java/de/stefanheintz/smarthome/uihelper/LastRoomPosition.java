package de.stefanheintz.smarthome.uihelper;

import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.GridViewPager;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class LastRoomPosition {

    public static void setViewToLastRoomPosition(GridViewPager pager, Map<SmartHomeLocation, List<SmartHomeDevice>> map, SharedPreferences settings) {
        String favLocation = settings.getString(Settings.PREF_FAV_LOCATION, "All");
        int row = 0;
        int index = 0;
        for (SmartHomeLocation smartHomeLocation : map.keySet()) {
            if (smartHomeLocation.getName() != null && smartHomeLocation.getName().equalsIgnoreCase(favLocation)) {
                row = index;
            }
            index++;
        }
        try {
            if (pager.getAdapter().getRowCount() >= row && pager.getAdapter().getColumnCount(row) >= 1) {
                pager.setCurrentItem(row, 1, false);
            }
        } catch (Exception e) {
            //forget it
        }
    }

    public static void saveLastRoomPosition(GridViewPager pager, Map<SmartHomeLocation, List<SmartHomeDevice>> roomList, SharedPreferences settings) {
        if(roomList.size() > 0) {
            SharedPreferences.Editor editor = settings.edit();
            int rowCount = pager.getCurrentItem().y;
            SmartHomeLocation o = (SmartHomeLocation) roomList.keySet().toArray()[rowCount];
            editor.putString(Settings.PREF_FAV_LOCATION, o.getName());
            editor.commit();
        }
    }
}
