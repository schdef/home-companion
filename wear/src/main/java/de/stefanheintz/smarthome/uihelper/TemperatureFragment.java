package de.stefanheintz.smarthome.uihelper;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.stefanheintz.smarthome.R;
import de.stefanheintz.smarthome.WearMainActivity;
import de.stefanheintz.smarthome.common.TemperatureUtil;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class TemperatureFragment extends Fragment implements View.OnClickListener, BaseFragment {

    private static Listener mListener;
    public TemperatureDevice device;
    private TextView vTemp1;
    private TextView vTemp2;
    private TextView vLabel;
    private Activity activity;

    public static TemperatureFragment create(TemperatureDevice device, Listener listener) {
        mListener = listener;
        TemperatureFragment fragment = new TemperatureFragment();
        Bundle args = new Bundle();
        args.putParcelable("DEVICE", device);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.temperature_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vLabel = (TextView) view.findViewById(R.id.label);
        vTemp1 = (TextView) view.findViewById(R.id.temp1);
        vTemp2 = (TextView) view.findViewById(R.id.temp2);

        device = getArguments().getParcelable("DEVICE");
        update(device);

        view.setOnClickListener(this);
        vTemp1.setOnClickListener(this);
        vTemp2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(mListener != null) {
            mListener.onActionPerformed();
        }
    }

    @Override
    public void update(SmartHomeDevice newDevice) {
        device = (TemperatureDevice) newDevice;
        String temperatureFormatted = device.getTemperatureFormatted();

        vLabel.setText(device.getAliasOrName());
        vTemp1.setText(temperatureFormatted + TemperatureUtil.DEGREE_SIGN);
        if(device.getHumidity() != null) {
            vTemp2.setText(device.getHumidity()+"%");
        } else {
            vTemp2.setVisibility(View.GONE);
        }

        TemperatureUtil.configureTemperatureColor(activity.getResources(), vTemp1, temperatureFormatted);
    }

    @Override
    public SmartHomeDevice getDevice() {
        return device;
    }

    public interface Listener {
        void onActionPerformed();
    }
}