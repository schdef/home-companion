package de.stefanheintz.smarthome;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.ArrayList;
import java.util.List;

import de.stefanheintz.smarthome.common.Settings;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.util.BackendCommunication;

/**
 * The MIT License (MIT)

 * Copyright (c) 2016 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class ListenerService extends WearableListenerService {

    private static final String TAG = "ListenerService";

    public SharedPreferences settings;

    @Override
    public void onCreate() {
        super.onCreate();
        settings = getSharedPreferences(WearMainActivity.PREF_HOCO_SETTINGS, MODE_PRIVATE);
        Log.v(TAG, "service created");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.v(TAG, "service bound");
        return super.onUnbind(intent);
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.v(TAG, "received onDataChanged");
        for (DataEvent event : dataEvents) {
            DataItem item = event.getDataItem();
            if (item.getUri().getPath().compareTo("/devicelist") == 0) {
                logDebug("Received device list from smartphone");
                DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                //TODO updatePreferenceSettings(dataMap);
                final ArrayList<DataMap> deviceDataMapList = dataMap.getDataMapArrayList("devicelist");
                if (deviceDataMapList != null) {
                    Log.v(TAG, "Received object list deviceDataMapList with count: " + deviceDataMapList.size());
                    final ArrayList<SmartHomeDevice> list = BackendCommunication.getAsObjectList(deviceDataMapList);
                    ((WearApplication)getApplication()).cacheDeviceList(settings, (List<SmartHomeDevice>) list.clone());
                    refreshView(null);
                }

            } else if (item.getUri().getPath().compareTo("/GETDEVICE_RESPONSE") == 0) {
                logDebug("Received device from smartphone");
                DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                final DataMap deviceDataMap = dataMap.getDataMap("device");
                if (deviceDataMap != null) {
                    Log.v(TAG, "Received device object");
                    final SmartHomeDevice device = BackendCommunication.getAsObject(deviceDataMap);
                    refreshView(device);
                }

            } else if (item.getUri().getPath().compareTo("/SETTINGS_RESPONSE") == 0) {
                Log.v(TAG, "Received settings list");
                DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                updatePreferenceSettings(dataMap);
                refreshView(null);
            }
        }
    }

    private void refreshView(SmartHomeDevice device) {
        Intent messageIntent = new Intent();
        messageIntent.setAction(Intent.ACTION_SEND);
        messageIntent.putExtra("device", (Parcelable) device);
        LocalBroadcastManager.getInstance(this).sendBroadcast(messageIntent);
    }

    private void logDebug(String text) {
        Log.v(TAG, text);
    }

    private void updatePreferenceSettings(DataMap dataMap) {
        String pref_key_no_full_or_list_view = dataMap.getString(Settings.PREF_KEY_NO_FULL_OR_LIST_VIEW);
        boolean pref_key_en_dis_debug_mode = dataMap.getBoolean(Settings.PREF_KEY_EN_DIS_DEBUG_MODE);
        String prefKeyRoomFilter = dataMap.getString(Settings.PREF_KEY_ROOM_FILTER);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Settings.PREF_KEY_NO_FULL_OR_LIST_VIEW, pref_key_no_full_or_list_view);
        editor.putBoolean(Settings.PREF_KEY_EN_DIS_DEBUG_MODE, pref_key_en_dis_debug_mode);
        editor.putString(Settings.PREF_KEY_ROOM_FILTER, prefKeyRoomFilter);
        editor.commit();
    }
}
