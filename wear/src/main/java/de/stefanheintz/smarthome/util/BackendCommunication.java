package de.stefanheintz.smarthome.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.List;

import de.stefanheintz.smarthome.WearMainActivity;
import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class BackendCommunication {

    private static final String TAG = "BackendCommunication";

    public static ArrayList<SmartHomeDevice> getAsObjectList(ArrayList<DataMap> deviceDataMapList) {
        ArrayList<SmartHomeDevice> result = new ArrayList<>();
        for (DataMap dataMap : deviceDataMapList) {
            result.add(getAsObject(dataMap));
        }
        return result;
    }
    public static SmartHomeDevice getAsObject(DataMap dataMap) {
        int deviceType = dataMap.getInt(SmartHomeDevice.KEY_TYPE);
        if (SmartHomeDevice.TEMPERATURE == deviceType) {
            return new TemperatureDevice(dataMap);
        } else if (SmartHomeDevice.TEMPERATURE_HEATING == deviceType) {
            return new TemperatureHeatingDevice(dataMap);
        } else if (SmartHomeDevice.RELAY == deviceType) {
            return new RelayDevice(dataMap);
        } else {
            return new SwitchDevice(dataMap);
        }
    }

    private final GoogleApiClient mGoogleApiClient;
    private Context context;

    public BackendCommunication(GoogleApiClient mGoogleApiClient, Context context) {
        this.mGoogleApiClient = mGoogleApiClient;
        this.context = context;
    }

    public String triggerDeviceAction(SmartHomeDevice theDevice) {
        String command = theDevice.getName() + "~" + theDevice.getLocationForSwitchOrEmptyString();
        if(theDevice instanceof TemperatureHeatingDevice) {
            command += "~"+((TemperatureHeatingDevice)theDevice).getDesiredTemperature();
        }
        callBackend("/MESSAGE", command);
        return command;
    }

    public void refreshAll() {
        callBackend("/GETDEVICELIST", "");
    }

    public void refreshDevice(SmartHomeDevice device) {
        callBackend("/GETDEVICE", device.getName());
    }

    public void refreshSettings() {
        callBackend("/GETSETTINGS", "");
    }

    private void callBackend(final String messageType, final CharSequence message) {
        Log.v(TAG, "call backend with messageType: " + messageType + ", message: " + message);
        if (mGoogleApiClient == null) {
            return;
        }
        final PendingResult<NodeApi.GetConnectedNodesResult> nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient);
        nodes.setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult result) {
                final List<Node> nodes = result.getNodes();
                if (nodes != null && nodes.size() > 0) {
                    for (int i = 0; i < nodes.size(); i++) {
                        final Node node = nodes.get(i);
                        Wearable.MessageApi.sendMessage(mGoogleApiClient, node.getId(), messageType, message.toString().getBytes()).setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                if (!sendMessageResult.getStatus().isSuccess()) {
                                    Log.v(TAG, "not successful send message to backend");
                                    Toast.makeText(context, "Communication error with handheld device!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Log.v(TAG, "successful send message to backend");
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(context, "No connection to handheld device!", Toast.LENGTH_SHORT).show();
                }
            }
            //}, 15, TimeUnit.SECONDS);
        });
    }
}
