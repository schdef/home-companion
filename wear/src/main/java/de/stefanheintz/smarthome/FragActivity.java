package de.stefanheintz.smarthome;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.io.Serializable;

import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;
import de.stefanheintz.smarthome.uihelper.RelayFragment;
import de.stefanheintz.smarthome.uihelper.TemperatureFragment;
import de.stefanheintz.smarthome.uihelper.TemperatureHeatingFragment;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
public class FragActivity extends Activity {

    private static final int CONTENT_VIEW_ID = 10101010;
    WearApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SmartHomeDevice device = (SmartHomeDevice) getIntent().getParcelableExtra("DEVICE");

        FrameLayout frame = new FrameLayout(this);
        frame.setId(CONTENT_VIEW_ID);
        setContentView(frame, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        application = (WearApplication) getApplication();

        if (savedInstanceState == null) {
            Fragment newFragment = null;
            if(device instanceof TemperatureHeatingDevice) {
                newFragment = TemperatureHeatingFragment.create(((TemperatureHeatingDevice) device), new TemperatureHeatingFragment.Listener() {
                    @Override
                    public void onActionPerformed(final TemperatureHeatingDevice theDevice) {
                        //Intent intent = new Intent(getApplicationContext(), WearWaitingScreen.class);
                        //startActivity(intent);

                        application.getBackendCommunication().triggerDeviceAction(theDevice);
                        finish();
                    }
                });
            } else if(device instanceof TemperatureDevice) {
                TemperatureDevice tDevice = (TemperatureDevice) device;
                newFragment = TemperatureFragment.create(tDevice, new TemperatureFragment.Listener() {
                    @Override
                    public void onActionPerformed() {
                        finish();
                    }
                });
            } else if(device instanceof RelayDevice) {
                RelayDevice relayDevice = (RelayDevice) device;
                newFragment = RelayFragment.create(relayDevice, new RelayFragment.Listener() {
                    @Override
                    public void onActionPerformed() {
                        finish();
                    }
                });
            }
            if(newFragment == null) {
                finish();
            }
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(CONTENT_VIEW_ID, newFragment).commit();
        }
    }
}
