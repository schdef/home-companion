package de.stefanheintz.smarthome;

import android.app.Application;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.util.BackendCommunication;
import de.stefanheintz.smarthome.util.SerializableUtil;

/**
 * Created by Stefan on 09.10.2015.
 */
public class WearApplication extends Application {

    private BackendCommunication backendCommunication;
    private List<SmartHomeDevice> cachedDeviceList;

    public BackendCommunication getBackendCommunication() {
        return this.backendCommunication;
    }
    public void setBackendCommunication(BackendCommunication backendCommunication) {
        this.backendCommunication = backendCommunication;
    }

    public void cacheDeviceList(SharedPreferences settings, List<SmartHomeDevice> smartHomeDevices) {
        this.cachedDeviceList = smartHomeDevices;
        try {
            String serializedObject = SerializableUtil.serializeObject(smartHomeDevices);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("cache", String.valueOf(serializedObject));
            editor.commit();
        } catch (Exception e) {
        }
    }


    public List<SmartHomeDevice> getCachedDeviceList(SharedPreferences settings) {
        if(cachedDeviceList != null) {
            return cachedDeviceList;
        } else {
            try {
                String cachedDevicesInSettings = settings.getString("cache", null);
                cachedDeviceList = (List<SmartHomeDevice>) SerializableUtil.deserializeObject(cachedDevicesInSettings);
                return cachedDeviceList;
            } catch (Exception e) {
            }
            return new ArrayList<>();
        }
    }
}
