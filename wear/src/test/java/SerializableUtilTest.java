

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.List;

import de.stefanheintz.smarthome.dto.RelayDevice;
import de.stefanheintz.smarthome.dto.SmartHomeDevice;
import de.stefanheintz.smarthome.dto.SmartHomeLocation;
import de.stefanheintz.smarthome.dto.SwitchDevice;
import de.stefanheintz.smarthome.dto.TemperatureDevice;
import de.stefanheintz.smarthome.dto.TemperatureHeatingDevice;
import de.stefanheintz.smarthome.util.SerializableUtil;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * The MIT License (MIT)

 * Copyright (c) 2015 Stefan Heintz

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
@RunWith(RobolectricTestRunner.class)
public class SerializableUtilTest {

    private List<SmartHomeDevice> smartDevices;

    @Before
    public void before() {
        smartDevices = new ArrayList<SmartHomeDevice>();

        TemperatureDevice temp = new TemperatureDevice("Test1", new SmartHomeLocation("Room1"), "off");
        temp.setTemperature("12.2");
        smartDevices.add(temp);
        smartDevices.add(new TemperatureHeatingDevice("Test2", new SmartHomeLocation("Room1"), "off"));
        smartDevices.add(new SwitchDevice("Test1", new SmartHomeLocation("Room2"), "on"));
        smartDevices.add(new RelayDevice("Test2", new SmartHomeLocation("Room2"), "on"));
    }

    @Test
    public void convertSuccessfulToBytStream() throws Exception {
        String serialize = SerializableUtil.serializeObject(smartDevices);
        assertNotNull(serialize);
        List<SmartHomeDevice> result = (List<SmartHomeDevice>) SerializableUtil.deserializeObject(serialize);
        assertNotNull(result);
        assertEquals(smartDevices, result);
    }
}

